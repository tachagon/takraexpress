require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Rails5App
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    config.middleware.use Rack::Cors do
    	allow do
    		origins '*'
    		resource '*',
    			:headers 	=> :any,
    			:expose 	=> ['access-token', 'expiry', 'token-type', 'uid', 'client'],
    			:methods 	=> [:get, :post, :options, :delete, :put]
    	end
    end

    config.time_zone = "Bangkok"

  end
end
