Rails.application.routes.draw do
  namespace :api do
  	namespace :v1 do
  		resources :roles
  		resources :users
  		resources :categories
  		resources :products
      resources :options
      resources :provinces, only: [:index, :show]
      resources :prefectures, only: [:index, :show]
      resources :districts, only: [:index, :show]
      resources :zipcodes, only: [:index, :show]
  	end
  end
  mount_devise_token_auth_for 'User', at: 'auth'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
