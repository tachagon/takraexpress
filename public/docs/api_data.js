define({ "api": [
  {
    "type": "post",
    "url": "/auth/sign_in",
    "title": "Sign in",
    "version": "1.0.0",
    "name": "SignIn",
    "group": "Auth",
    "description": "<p>sign in เพื่อขอสิทธิ์ในการเข้าถึง resources ของระบบ</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email ของ user ที่ต้องการเข้าสู่ระบบ</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>รหัสผ่านของ user ที่ต้องการเข้าสู่ระบบ</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Param:",
          "content": "{\n  \"email\": \"email@example.com\",\n  \"password\": \"password\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<p>ข้อมูลที่ส่งมาไม่ถูกต้องจึงไม่สามารถเข้าสู่ระบบได้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Unauthorized-Error:",
          "content": "HTTP/1.1 401 Unauthorized\n{\n  \"errors\": [\n    \"Invalid login credentials. Please try again.\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"data\": {\n    \"id\": 1,\n    \"email\": \"o_k_t@hotmail.com\",\n    \"provider\": \"email\",\n    \"role_id\": 3,\n    \"name\": \"Tatchagon Koonkoei\",\n    \"uid\": \"o_k_t@hotmail.com\",\n    \"nickname\": null,\n    \"image\": null,\n    \"birthday\": \"1993-06-02\",\n    \"gender\": \"male\",\n    \"phone_number\": \"0826810461\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/auth_apidoc.rb",
    "groupTitle": "Auth"
  },
  {
    "type": "delete",
    "url": "/auth/sign_out",
    "title": "Sign out",
    "version": "1.0.0",
    "name": "SignOut",
    "group": "Auth",
    "description": "<p>sign out เพื่อทำให้ user ออกจากการใช้งานระบบ</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา user ได้เพราะผู้ใช้ยังไม่ได้เข้าสู่ระบบ หรือข้อมูลใน headers ไม่ถูกต้อง</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"errors\": [\n    \"User was not found or was not logged in.\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"success\": true\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/auth_apidoc.rb",
    "groupTitle": "Auth"
  },
  {
    "type": "post",
    "url": "/api/v1/categories/",
    "title": "Create a Category",
    "version": "1.0.0",
    "name": "CreateACategory",
    "group": "Category",
    "description": "<p>สร้าง category ใหม่ โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Category Param": [
          {
            "group": "Category Param",
            "type": "String",
            "optional": false,
            "field": "category[name]",
            "description": "<p>ชื่อของ category</p>"
          },
          {
            "group": "Category Param",
            "type": "String",
            "optional": true,
            "field": "category[description]",
            "description": "<p>คำอธิบายของ category</p>"
          },
          {
            "group": "Category Param",
            "type": "String",
            "optional": true,
            "field": "category[image]",
            "description": "<p>ที่อยู่ภาพของ category</p>"
          },
          {
            "group": "Category Param",
            "type": "Integer",
            "optional": true,
            "field": "category[supercategory_id]",
            "description": "<p>id ของ parent category</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Product-Param:",
          "content": "{\n  \"category\" : {\n    \"name\": \"some category\",\n    \"description\": \"some content\",\n    \"image\": \"some where\",\n    \"supercategory_id\": 1\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่ม Category ใหม่ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Validation-Error:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"name\": [\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 Created\n{\n  \"id\": 10,\n  \"name\": \"some category\",\n  \"description\": \"some content\",\n  \"image\": \"some where\",\n  \"created_at\": \"2016-12-03T23:18:30+07:00\",\n  \"updated_at\": \"2016-12-03T23:18:30+07:00\",\n  \"supercategory\": {\n    \"id\": 1,\n    \"name\": \"อิเล็กทรอนิกส์\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-03T22:52:25+07:00\",\n    \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n  },\n  \"subcategories\": [],\n  \"products\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/categories_apidoc.rb",
    "groupTitle": "Category"
  },
  {
    "type": "delete",
    "url": "/api/v1/categories/:id",
    "title": "Destroy a category",
    "version": "1.0.0",
    "name": "DestroyACategory",
    "group": "Category",
    "description": "<p>ลบข้อมูล category โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา category ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/categories_apidoc.rb",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "/api/v1/categories/:id",
    "title": "Get a category",
    "version": "1.0.0",
    "name": "GetACategory",
    "group": "Category",
    "description": "<p>เรียกดูข้อมูลของ category ตาม id</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ Category</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อของ Category</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>คำอธิบายของ Category</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>ที่อยู่ภาพของ Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "supercategory",
            "description": "<p>Object ของ parent category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "subcategories",
            "description": "<p>Object Array ของ child category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "products",
            "description": "<p>Object Array ของ product ทั้งหมดใน category นั้น</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"name\": \"อิเล็กทรอนิกส์\",\n  \"description\": \"\",\n  \"image\": \"\",\n  \"created_at\": \"2016-12-03T22:52:25+07:00\",\n  \"updated_at\": \"2016-12-03T22:52:25+07:00\",\n  \"supercategory\": null,\n  \"subcategories\": [\n    {\n      \"id\": 2,\n      \"name\": \"โทรศัพท์มือถือ & แท็บเล็ต\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-03T22:52:25+07:00\",\n      \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n    }\n  ],\n  \"products\": [\n    {\n      \"id\": 1,\n      \"name\": \"Sky Coffee\",\n      \"description\": \"content\",\n      \"unit\": \"box\",\n      \"created_at\": \"2016-12-03T22:52:26+07:00\",\n      \"updated_at\": \"2016-12-03T22:52:26+07:00\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา category ที่ตรงกับ <code>id</code> ได้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/categories_apidoc.rb",
    "groupTitle": "Category"
  },
  {
    "type": "get",
    "url": "/api/v1/categories",
    "title": "Get all categories",
    "version": "1.0.0",
    "name": "GetCategories",
    "group": "Category",
    "description": "<p>เรียกดูข้อมูลของประเภทสินค้าทั้งหมด</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ Category</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อของ Category</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>คำอธิบายของ Category</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>ที่อยู่ภาพของ Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "supercategory",
            "description": "<p>Object ของ parent category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "subcategories",
            "description": "<p>Object Array ของ child category</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "products",
            "description": "<p>Object Array ของ product ทั้งหมดใน category นั้น</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n[\n  {\n    \"id\": 1,\n    \"name\": \"อิเล็กทรอนิกส์\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-03T22:52:25+07:00\",\n    \"updated_at\": \"2016-12-03T22:52:25+07:00\",\n    \"supercategory\": null,\n    \"subcategories\": [\n      {\n        \"id\": 2,\n        \"name\": \"โทรศัพท์มือถือ & แท็บเล็ต\",\n        \"description\": \"\",\n        \"image\": \"\",\n        \"created_at\": \"2016-12-03T22:52:25+07:00\",\n        \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n      }\n    ],\n    \"products\": [\n      {\n        \"id\": 1,\n        \"name\": \"Sky Coffee\",\n        \"description\": \"content\",\n        \"unit\": \"box\",\n        \"created_at\": \"2016-12-03T22:52:26+07:00\",\n        \"updated_at\": \"2016-12-03T22:52:26+07:00\"\n      }\n    ]\n  },\n  {\n    \"id\": 2,\n    \"name\": \"โทรศัพท์มือถือ & แท็บเล็ต\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-03T22:52:25+07:00\",\n    \"updated_at\": \"2016-12-03T22:52:25+07:00\",\n    \"supercategory\": {\n      \"id\": 1,\n      \"name\": \"อิเล็กทรอนิกส์\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-03T22:52:25+07:00\",\n      \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n    },\n    \"subcategories\": [\n      {\n        \"id\": 3,\n        \"name\": \"โทรศัพท์มือถือ\",\n        \"description\": \"\",\n        \"image\": \"\",\n        \"created_at\": \"2016-12-03T22:52:25+07:00\",\n        \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n      },\n      {\n        \"id\": 4,\n        \"name\": \"แท็บเล็ต\",\n        \"description\": \"\",\n        \"image\": \"\",\n        \"created_at\": \"2016-12-03T22:52:25+07:00\",\n        \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n      },\n      {\n        \"id\": 5,\n        \"name\": \"อุปกรณ์เสริมโทรศัพท์มือถือ\",\n        \"description\": \"\",\n        \"image\": \"\",\n        \"created_at\": \"2016-12-03T22:52:25+07:00\",\n        \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n      }\n    ],\n    \"products\": [\n      {\n        \"id\": 2,\n        \"name\": \"3D Nano\",\n        \"description\": \"content\",\n        \"unit\": \"can\",\n        \"created_at\": \"2016-12-03T22:52:26+07:00\",\n        \"updated_at\": \"2016-12-03T22:52:26+07:00\"\n      }\n    ]\n  },\n  {\n    \"id\": 3,\n    \"name\": \"โทรศัพท์มือถือ\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-03T22:52:25+07:00\",\n    \"updated_at\": \"2016-12-03T22:52:25+07:00\",\n    \"supercategory\": {\n      \"id\": 2,\n      \"name\": \"โทรศัพท์มือถือ & แท็บเล็ต\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-03T22:52:25+07:00\",\n      \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n    },\n    \"subcategories\": [],\n    \"products\": []\n  },\n  {\n    \"id\": 4,\n    \"name\": \"แท็บเล็ต\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-03T22:52:25+07:00\",\n    \"updated_at\": \"2016-12-03T22:52:25+07:00\",\n    \"supercategory\": {\n      \"id\": 2,\n      \"name\": \"โทรศัพท์มือถือ & แท็บเล็ต\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-03T22:52:25+07:00\",\n      \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n    },\n    \"subcategories\": [],\n    \"products\": []\n  },\n  {\n    \"id\": 5,\n    \"name\": \"อุปกรณ์เสริมโทรศัพท์มือถือ\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-03T22:52:25+07:00\",\n    \"updated_at\": \"2016-12-03T22:52:25+07:00\",\n    \"supercategory\": {\n      \"id\": 2,\n      \"name\": \"โทรศัพท์มือถือ & แท็บเล็ต\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-03T22:52:25+07:00\",\n      \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n    },\n    \"subcategories\": [\n      {\n        \"id\": 6,\n        \"name\": \"แบตเตอรีสำรอง\",\n        \"description\": \"\",\n        \"image\": \"\",\n        \"created_at\": \"2016-12-03T22:52:25+07:00\",\n        \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n      },\n      {\n        \"id\": 7,\n        \"name\": \"แบตเตอรี และอุปกรณ์ชาร์จไฟ\",\n        \"description\": \"\",\n        \"image\": \"\",\n        \"created_at\": \"2016-12-03T22:52:25+07:00\",\n        \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n      }\n    ],\n    \"products\": []\n  },\n  {\n    \"id\": 6,\n    \"name\": \"แบตเตอรีสำรอง\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-03T22:52:25+07:00\",\n    \"updated_at\": \"2016-12-03T22:52:25+07:00\",\n    \"supercategory\": {\n      \"id\": 5,\n      \"name\": \"อุปกรณ์เสริมโทรศัพท์มือถือ\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-03T22:52:25+07:00\",\n      \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n    },\n    \"subcategories\": [],\n    \"products\": []\n  },\n  {\n    \"id\": 7,\n    \"name\": \"แบตเตอรี และอุปกรณ์ชาร์จไฟ\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-03T22:52:25+07:00\",\n    \"updated_at\": \"2016-12-03T22:52:25+07:00\",\n    \"supercategory\": {\n      \"id\": 5,\n      \"name\": \"อุปกรณ์เสริมโทรศัพท์มือถือ\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-03T22:52:25+07:00\",\n      \"updated_at\": \"2016-12-03T22:52:25+07:00\"\n    },\n    \"subcategories\": [],\n    \"products\": []\n  }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/categories_apidoc.rb",
    "groupTitle": "Category"
  },
  {
    "type": "put",
    "url": "/api/v1/categories/:id",
    "title": "Update a category",
    "version": "1.0.0",
    "name": "UpdateACategory",
    "group": "Category",
    "description": "<p>แก้ไขข้อมูล category โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Category Param": [
          {
            "group": "Category Param",
            "type": "String",
            "optional": false,
            "field": "category[name]",
            "description": "<p>ชื่อของ category</p>"
          },
          {
            "group": "Category Param",
            "type": "String",
            "optional": true,
            "field": "category[description]",
            "description": "<p>คำอธิบายของ category</p>"
          },
          {
            "group": "Category Param",
            "type": "String",
            "optional": true,
            "field": "category[image]",
            "description": "<p>ที่อยู่ภาพของ category</p>"
          },
          {
            "group": "Category Param",
            "type": "Integer",
            "optional": true,
            "field": "category[supercategory_id]",
            "description": "<p>id ของ parent category</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Product-Param:",
          "content": "{\n  \"category\" : {\n    \"name\": \"some category\",\n    \"description\": \"some content\",\n    \"image\": \"some where\",\n    \"supercategory_id\": 1\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา category ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล Category ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"name\": [\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 10,\n  \"name\": \"edit category\",\n  \"description\": \"des bla bla bla\",\n  \"image\": \"img some where\",\n  \"created_at\": \"2016-12-03T23:18:30+07:00\",\n  \"updated_at\": \"2016-12-03T23:42:49+07:00\",\n  \"supercategory\": null,\n  \"subcategories\": [],\n  \"products\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/categories_apidoc.rb",
    "groupTitle": "Category"
  },
  {
    "type": "post",
    "url": "/api/v1/options/",
    "title": "Create a option",
    "version": "1.0.0",
    "name": "CreateAOption",
    "group": "Option",
    "description": "<p>สร้าง option ใหม่ โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็นเจ้าของสินค้าหรือผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Option Param": [
          {
            "group": "Option Param",
            "type": "String",
            "optional": false,
            "field": "option[name]",
            "description": "<p>ชื่อของตัวเลือกสินค้า</p>"
          },
          {
            "group": "Option Param",
            "type": "Float",
            "optional": false,
            "field": "option[price]",
            "description": "<p>ราคาสินค้าของตัวเลือกสินค้านี้</p>"
          },
          {
            "group": "Option Param",
            "type": "Integer",
            "optional": false,
            "field": "option[quantity]",
            "description": "<p>จำนวนสินค้าของตัวเลือกสินค้านี้</p>"
          },
          {
            "group": "Option Param",
            "type": "Float",
            "optional": true,
            "field": "option[weight]",
            "description": "<p>น้ำหนักสินค้าของตัวเลือกสินค้านี้</p>"
          },
          {
            "group": "Option Param",
            "type": "Integer",
            "optional": false,
            "field": "option[product_id]",
            "description": "<p>สินค้าที่เป็นเจ้าของตัวเลือกสินค้านี้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Product-Param:",
          "content": "{\n  \"option\" : {\n    \"name\": \"some option\",\n    \"price\": 99,\n    \"quantity\": 25,\n    \"weight\": 100,\n    \"product_id\": 1\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่ม Option ใหม่ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่มีสิทธิ์ในการดำเนินการนี้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Validation-Error:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"product\": [\n    \"must exist\"\n  ],\n  \"name\": [\n    \"can't be blank\"\n  ],\n  \"price\": [\n    \"is not a number\",\n    \"can't be blank\"\n  ],\n  \"quantity\": [\n    \"is not a number\",\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 Created\n{\n  \"id\": 8,\n  \"name\": \"some option\",\n  \"price\": 99,\n  \"quantity\": 25,\n  \"weight\": 100,\n  \"created_at\": \"2016-12-04T21:15:28+07:00\",\n  \"updated_at\": \"2016-12-04T21:15:28+07:00\",\n  \"product\": {\n    \"id\": 1,\n    \"name\": \"Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook\",\n    \"description\": \"content\",\n    \"unit\": \"ชิ้น\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/options_apidoc.rb",
    "groupTitle": "Option"
  },
  {
    "type": "delete",
    "url": "/api/v1/options/:id",
    "title": "Destroy a Option",
    "version": "1.0.0",
    "name": "DestroyAOption",
    "group": "Option",
    "description": "<p>ลบข้อมูล option โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็นเจ้าของสินค้าหรือผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา option ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่มีสิทธิ์ในการดำเนินการนี้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/options_apidoc.rb",
    "groupTitle": "Option"
  },
  {
    "type": "get",
    "url": "/api/v1/options/:id",
    "title": "Get a option",
    "version": "1.0.0",
    "name": "GetAOption",
    "group": "Option",
    "description": "<p>เรียกดูข้อมูลของ option ตาม id</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ Option</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อของ Option</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "price",
            "description": "<p>ราคาสินค้าในตัวเลือกนั้น</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "quantity",
            "description": "<p>จำนวนสินค้าในตัวเลือกนั้น</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "weight",
            "description": "<p>น้ำหนักของสินค้า</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง Option</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Option</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Object ของสินค้าที่เป็นเจ้าของตัวเลือก</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"name\": \"สีขาว\",\n  \"price\": 650,\n  \"quantity\": 10,\n  \"weight\": 500,\n  \"created_at\": \"2016-12-04T14:36:17+07:00\",\n  \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n  \"product\": {\n    \"id\": 1,\n    \"name\": \"Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook\",\n    \"description\": \"content\",\n    \"unit\": \"ชิ้น\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา option ที่ตรงกับ <code>id</code> ได้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/options_apidoc.rb",
    "groupTitle": "Option"
  },
  {
    "type": "get",
    "url": "/api/v1/options",
    "title": "Get all options",
    "version": "1.0.0",
    "name": "GetOptions",
    "group": "Option",
    "description": "<p>เรียกดูข้อมูลของตัวเลือกสินค้าทั้งหมด</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ Option</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อของ Option</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "price",
            "description": "<p>ราคาสินค้าในตัวเลือกนั้น</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "quantity",
            "description": "<p>จำนวนสินค้าในตัวเลือกนั้น</p>"
          },
          {
            "group": "Success 200",
            "type": "Float",
            "optional": false,
            "field": "weight",
            "description": "<p>น้ำหนักของสินค้า</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง Option</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Option</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "product",
            "description": "<p>Object ของสินค้าที่เป็นเจ้าของตัวเลือก</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n    \"id\": 1,\n    \"name\": \"สีขาว\",\n    \"price\": 650,\n    \"quantity\": 10,\n    \"weight\": 500,\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"product\": {\n      \"id\": 1,\n      \"name\": \"Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook\",\n      \"description\": \"content\",\n      \"unit\": \"ชิ้น\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    }\n  },\n  {\n    \"id\": 2,\n    \"name\": \"สีดำ\",\n    \"price\": 650,\n    \"quantity\": 13,\n    \"weight\": 500,\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"product\": {\n      \"id\": 1,\n      \"name\": \"Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook\",\n      \"description\": \"content\",\n      \"unit\": \"ชิ้น\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    }\n  },\n  {\n    \"id\": 3,\n    \"name\": \"White Gold\",\n    \"price\": 8250,\n    \"quantity\": 20,\n    \"weight\": 167,\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"product\": {\n      \"id\": 2,\n      \"name\": \"Samsung Galaxy J7 Prime\",\n      \"description\": \"content\",\n      \"unit\": \"เครื่อง\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    }\n  },\n  {\n    \"id\": 4,\n    \"name\": \"Black\",\n    \"price\": 8250,\n    \"quantity\": 12,\n    \"weight\": 167,\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"product\": {\n      \"id\": 2,\n      \"name\": \"Samsung Galaxy J7 Prime\",\n      \"description\": \"content\",\n      \"unit\": \"เครื่อง\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    }\n  },\n  {\n    \"id\": 5,\n    \"name\": \"Pink Gold\",\n    \"price\": 8250,\n    \"quantity\": 15,\n    \"weight\": 167,\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"product\": {\n      \"id\": 2,\n      \"name\": \"Samsung Galaxy J7 Prime\",\n      \"description\": \"content\",\n      \"unit\": \"เครื่อง\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    }\n  },\n  {\n    \"id\": 6,\n    \"name\": \"size M\",\n    \"price\": 199,\n    \"quantity\": 10,\n    \"weight\": null,\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"product\": {\n      \"id\": 3,\n      \"name\": \"เดรสยาวลายริ้ว คอกลม แขนสั้น\",\n      \"description\": \"content\",\n      \"unit\": \"ตัว\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    }\n  },\n  {\n    \"id\": 7,\n    \"name\": \"size L\",\n    \"price\": 199,\n    \"quantity\": 8,\n    \"weight\": null,\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"product\": {\n      \"id\": 3,\n      \"name\": \"เดรสยาวลายริ้ว คอกลม แขนสั้น\",\n      \"description\": \"content\",\n      \"unit\": \"ตัว\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    }\n  }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/options_apidoc.rb",
    "groupTitle": "Option"
  },
  {
    "type": "put",
    "url": "/api/v1/options/:id",
    "title": "Update a option",
    "version": "1.0.0",
    "name": "UpdateAOption",
    "group": "Option",
    "description": "<p>แก้ไขข้อมูล option โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็นเจ้าของสินค้าหรือผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Option Param": [
          {
            "group": "Option Param",
            "type": "String",
            "optional": false,
            "field": "option[name]",
            "description": "<p>ชื่อของตัวเลือกสินค้า</p>"
          },
          {
            "group": "Option Param",
            "type": "Float",
            "optional": false,
            "field": "option[price]",
            "description": "<p>ราคาสินค้าของตัวเลือกสินค้านี้</p>"
          },
          {
            "group": "Option Param",
            "type": "Integer",
            "optional": false,
            "field": "option[quantity]",
            "description": "<p>จำนวนสินค้าของตัวเลือกสินค้านี้</p>"
          },
          {
            "group": "Option Param",
            "type": "Float",
            "optional": true,
            "field": "option[weight]",
            "description": "<p>น้ำหนักสินค้าของตัวเลือกสินค้านี้</p>"
          },
          {
            "group": "Option Param",
            "type": "Integer",
            "optional": false,
            "field": "option[product_id]",
            "description": "<p>สินค้าที่เป็นเจ้าของตัวเลือกสินค้านี้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Product-Param:",
          "content": "{\n  \"option\" : {\n    \"name\": \"update option\",\n    \"price\": 100,\n    \"quantity\": 50,\n    \"weight\": 100,\n    \"product_id\": 1\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา option ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล option ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่มีสิทธิ์ในการดำเนินการนี้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"product\": [\n    \"must exist\"\n  ],\n  \"name\": [\n    \"can't be blank\"\n  ],\n  \"price\": [\n    \"is not a number\",\n    \"can't be blank\"\n  ],\n  \"quantity\": [\n    \"is not a number\",\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 8,\n  \"name\": \"update option\",\n  \"price\": 100,\n  \"quantity\": 50,\n  \"weight\": 100,\n  \"created_at\": \"2016-12-04T21:15:28+07:00\",\n  \"updated_at\": \"2016-12-04T21:36:58+07:00\",\n  \"product\": {\n    \"id\": 1,\n    \"name\": \"Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook\",\n    \"description\": \"content\",\n    \"unit\": \"ชิ้น\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/options_apidoc.rb",
    "groupTitle": "Option"
  },
  {
    "type": "post",
    "url": "/api/v1/products/",
    "title": "Create a product",
    "version": "1.0.0",
    "name": "CreateAProduct",
    "group": "Product",
    "description": "<p>สร้าง product ใหม่ โดยจะต้องมีการ sign in ก่อนจึงจะสามารถทำได้</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Product Param": [
          {
            "group": "Product Param",
            "type": "String",
            "optional": false,
            "field": "product[name]",
            "description": "<p>ชื่อของ Product</p>"
          },
          {
            "group": "Product Param",
            "type": "String",
            "optional": true,
            "field": "product[description]",
            "description": "<p>คำอธิบายของ Product</p>"
          },
          {
            "group": "Product Param",
            "type": "String",
            "optional": true,
            "field": "product[unit]",
            "description": "<p>ชื่อหน่วยของ Product เช่น กล่อง, กระป๋อง</p>"
          },
          {
            "group": "Product Param",
            "type": "Integer",
            "optional": false,
            "field": "product[user_id]",
            "description": "<p>id  ของ user ที่เป็นเจ้าของ Product</p>"
          },
          {
            "group": "Product Param",
            "type": "Integer",
            "optional": false,
            "field": "product[category_id]",
            "description": "<p>id  ของ category คือประเภทของสินค้า</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Product-Param:",
          "content": "{\n  \"product\" : {\n    \"name\": \"Sky Coffee\",\n    \"description\": \"content\",\n    \"unit\": \"box\",\n    \"user_id\": 1,\n    \"category_id\": 1\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่ม Product ใหม่ได้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Validation-Error:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"user\": [\n    \"must exist\"\n  ],\n  \"name\": [\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 Created\n{\n  \"id\": 4,\n  \"name\": \"new product\",\n  \"description\": \"content\",\n  \"unit\": \"unit\",\n  \"created_at\": \"2016-12-04T14:59:54+07:00\",\n  \"updated_at\": \"2016-12-04T14:59:54+07:00\",\n  \"user\": {\n    \"id\": 1,\n    \"provider\": \"email\",\n    \"uid\": \"o_k_t@hotmail.com\",\n    \"email\": \"o_k_t@hotmail.com\",\n    \"name\": \"Tatchagon Koonkoei\",\n    \"image\": null,\n    \"birthday\": \"1993-06-02\",\n    \"gender\": \"male\",\n    \"phone_number\": \"0826810461\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n  },\n  \"category\": {\n    \"id\": 1,\n    \"name\": \"อิเล็กทรอนิกส์\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n  },\n  \"options\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/products_apidoc.rb",
    "groupTitle": "Product"
  },
  {
    "type": "delete",
    "url": "/api/v1/products/:id",
    "title": "Destroy a product",
    "version": "1.0.0",
    "name": "DestroyAProduct",
    "group": "Product",
    "description": "<p>ลบข้อมูล product โดยจะต้องมีการ sign in ก่อนจึงจะสามารถทำได้</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา product ที่ตรงกับ <code>id</code> ได้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/products_apidoc.rb",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/api/v1/products/:id",
    "title": "Get a product",
    "version": "1.0.0",
    "name": "GetAProduct",
    "group": "Product",
    "description": "<p>เรียกดูข้อมูลของ product ตาม id</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ Product</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อของ Product</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>คำอธิบายของ Product</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": "<p>ชื่อหน่วยของ Product เช่น กล่อง, กระป๋อง</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง Product</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Product</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Object ของ user ที่เป็นเจ้าของ Product</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Object ของ category คือประเภทของสินค้า</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "options",
            "description": "<p>Object Array ของตัวเลือกสินค้า</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"name\": \"Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook\",\n  \"description\": \"content\",\n  \"unit\": \"ชิ้น\",\n  \"created_at\": \"2016-12-04T14:36:17+07:00\",\n  \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n  \"user\": {\n    \"id\": 1,\n    \"provider\": \"email\",\n    \"uid\": \"o_k_t@hotmail.com\",\n    \"email\": \"o_k_t@hotmail.com\",\n    \"name\": \"Tatchagon Koonkoei\",\n    \"image\": null,\n    \"birthday\": \"1993-06-02\",\n    \"gender\": \"male\",\n    \"phone_number\": \"0826810461\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n  },\n  \"category\": {\n    \"id\": 1,\n    \"name\": \"อิเล็กทรอนิกส์\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n  },\n  \"options\": [\n    {\n      \"id\": 1,\n      \"name\": \"สีขาว\",\n      \"price\": 650,\n      \"quantity\": 10,\n      \"weight\": 500,\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    },\n    {\n      \"id\": 2,\n      \"name\": \"สีดำ\",\n      \"price\": 650,\n      \"quantity\": 13,\n      \"weight\": 500,\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา product ที่ตรงกับ <code>id</code> ได้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/products_apidoc.rb",
    "groupTitle": "Product"
  },
  {
    "type": "get",
    "url": "/api/v1/products",
    "title": "Get all products",
    "version": "1.0.0",
    "name": "GetProducts",
    "group": "Product",
    "description": "<p>เรียกดูข้อมูลของ product ทั้งหมด</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ Product</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อของ Product</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>คำอธิบายของ Product</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "unit",
            "description": "<p>ชื่อหน่วยของ Product เช่น กล่อง, กระป๋อง</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง Product</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Product</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "user",
            "description": "<p>Object ของ user ที่เป็นเจ้าของ Product</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "category",
            "description": "<p>Object ของ category คือประเภทของสินค้า</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "options",
            "description": "<p>Object Array ของตัวเลือกสินค้า</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "  HTTP/1.1 200 OK\n[\n  {\n    \"id\": 1,\n    \"name\": \"Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook\",\n    \"description\": \"content\",\n    \"unit\": \"ชิ้น\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"user\": {\n      \"id\": 1,\n      \"provider\": \"email\",\n      \"uid\": \"o_k_t@hotmail.com\",\n      \"email\": \"o_k_t@hotmail.com\",\n      \"name\": \"Tatchagon Koonkoei\",\n      \"image\": null,\n      \"birthday\": \"1993-06-02\",\n      \"gender\": \"male\",\n      \"phone_number\": \"0826810461\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    },\n    \"category\": {\n      \"id\": 1,\n      \"name\": \"อิเล็กทรอนิกส์\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    },\n    \"options\": [\n      {\n        \"id\": 1,\n        \"name\": \"สีขาว\",\n        \"price\": 650,\n        \"quantity\": 10,\n        \"weight\": 500,\n        \"created_at\": \"2016-12-04T14:36:17+07:00\",\n        \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n      },\n      {\n        \"id\": 2,\n        \"name\": \"สีดำ\",\n        \"price\": 650,\n        \"quantity\": 13,\n        \"weight\": 500,\n        \"created_at\": \"2016-12-04T14:36:17+07:00\",\n        \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n      }\n    ]\n  },\n  {\n    \"id\": 2,\n    \"name\": \"Samsung Galaxy J7 Prime\",\n    \"description\": \"content\",\n    \"unit\": \"เครื่อง\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"user\": {\n      \"id\": 3,\n      \"provider\": \"email\",\n      \"uid\": \"leonado_davinci@email.com\",\n      \"email\": \"leonado_davinci@email.com\",\n      \"name\": \"Leonado Davinci\",\n      \"image\": null,\n      \"birthday\": \"1889-10-01\",\n      \"gender\": \"male\",\n      \"phone_number\": \"034658471\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    },\n    \"category\": {\n      \"id\": 2,\n      \"name\": \"โทรศัพท์มือถือ & แท็บเล็ต\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    },\n    \"options\": [\n      {\n        \"id\": 3,\n        \"name\": \"White Gold\",\n        \"price\": 8250,\n        \"quantity\": 20,\n        \"weight\": 167,\n        \"created_at\": \"2016-12-04T14:36:17+07:00\",\n        \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n      },\n      {\n        \"id\": 4,\n        \"name\": \"Black\",\n        \"price\": 8250,\n        \"quantity\": 12,\n        \"weight\": 167,\n        \"created_at\": \"2016-12-04T14:36:17+07:00\",\n        \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n      },\n      {\n        \"id\": 5,\n        \"name\": \"Pink Gold\",\n        \"price\": 8250,\n        \"quantity\": 15,\n        \"weight\": 167,\n        \"created_at\": \"2016-12-04T14:36:17+07:00\",\n        \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n      }\n    ]\n  },\n  {\n    \"id\": 3,\n    \"name\": \"เดรสยาวลายริ้ว คอกลม แขนสั้น\",\n    \"description\": \"content\",\n    \"unit\": \"ตัว\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\",\n    \"user\": {\n      \"id\": 1,\n      \"provider\": \"email\",\n      \"uid\": \"o_k_t@hotmail.com\",\n      \"email\": \"o_k_t@hotmail.com\",\n      \"name\": \"Tatchagon Koonkoei\",\n      \"image\": null,\n      \"birthday\": \"1993-06-02\",\n      \"gender\": \"male\",\n      \"phone_number\": \"0826810461\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    },\n    \"category\": {\n      \"id\": 9,\n      \"name\": \"แฟชั่นสุภาพสตรี\",\n      \"description\": \"\",\n      \"image\": \"\",\n      \"created_at\": \"2016-12-04T14:36:17+07:00\",\n      \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n    },\n    \"options\": [\n      {\n        \"id\": 6,\n        \"name\": \"size M\",\n        \"price\": 199,\n        \"quantity\": 10,\n        \"weight\": null,\n        \"created_at\": \"2016-12-04T14:36:17+07:00\",\n        \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n      },\n      {\n        \"id\": 7,\n        \"name\": \"size L\",\n        \"price\": 199,\n        \"quantity\": 8,\n        \"weight\": null,\n        \"created_at\": \"2016-12-04T14:36:17+07:00\",\n        \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n      }\n    ]\n  }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/products_apidoc.rb",
    "groupTitle": "Product"
  },
  {
    "type": "put",
    "url": "/api/v1/products/:id",
    "title": "Update a product",
    "version": "1.0.0",
    "name": "UpdateAProduct",
    "group": "Product",
    "description": "<p>แก้ไขข้อมูล product โดยจะต้องมีการ sign in ก่อนจึงจะสามารถทำได้</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Product Param": [
          {
            "group": "Product Param",
            "type": "String",
            "optional": false,
            "field": "product[name]",
            "description": "<p>ชื่อของ Product</p>"
          },
          {
            "group": "Product Param",
            "type": "String",
            "optional": true,
            "field": "product[description]",
            "description": "<p>คำอธิบายของ Product</p>"
          },
          {
            "group": "Product Param",
            "type": "String",
            "optional": true,
            "field": "product[unit]",
            "description": "<p>ชื่อหน่วยของ Product เช่น กล่อง, กระป๋อง</p>"
          },
          {
            "group": "Product Param",
            "type": "Integer",
            "optional": false,
            "field": "product[user_id]",
            "description": "<p>id  ของ user ที่เป็นเจ้าของ Product</p>"
          },
          {
            "group": "Product Param",
            "type": "Integer",
            "optional": false,
            "field": "product[category_id]",
            "description": "<p>id  ของ category คือประเภทของสินค้า</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Product-Param:",
          "content": "{\n  \"product\" : {\n    \"name\": \"Sky Coffee edit\",\n    \"description\": \"content edit\",\n    \"unit\": \"box edit\",\n    \"user_id\": 1,\n    \"category_id\": 1\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา product ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล Product ได้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"user\": [\n    \"must exist\"\n  ],\n  \"name\": [\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 4,\n  \"name\": \"update product\",\n  \"description\": \"content edit\",\n  \"unit\": \"unit\",\n  \"created_at\": \"2016-12-04T14:59:54+07:00\",\n  \"updated_at\": \"2016-12-04T15:01:26+07:00\",\n  \"user\": {\n    \"id\": 1,\n    \"provider\": \"email\",\n    \"uid\": \"o_k_t@hotmail.com\",\n    \"email\": \"o_k_t@hotmail.com\",\n    \"name\": \"Tatchagon Koonkoei\",\n    \"image\": null,\n    \"birthday\": \"1993-06-02\",\n    \"gender\": \"male\",\n    \"phone_number\": \"0826810461\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n  },\n  \"category\": {\n    \"id\": 1,\n    \"name\": \"อิเล็กทรอนิกส์\",\n    \"description\": \"\",\n    \"image\": \"\",\n    \"created_at\": \"2016-12-04T14:36:17+07:00\",\n    \"updated_at\": \"2016-12-04T14:36:17+07:00\"\n  },\n  \"options\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/products_apidoc.rb",
    "groupTitle": "Product"
  },
  {
    "type": "post",
    "url": "/api/v1/roles/",
    "title": "Create a role",
    "version": "1.0.0",
    "name": "CreateARole",
    "group": "Role",
    "description": "<p>เพิ่ม role ใหม่โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>Token is received from previous request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>Token is received from authentication.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>Email of current user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization-Header-Example:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Role Param": [
          {
            "group": "Role Param",
            "type": "String",
            "optional": false,
            "field": "role[name]",
            "description": "<p>Name of the Role.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Role-Param-Example:",
          "content": "{\n  \"role\" : {\n    \"name\": \"member\",\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่มข้อมูล role ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"name\": [\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 Created\n{\n  \"id\": 4,\n  \"name\": \"actor\",\n  \"created_at\": \"2016-12-01T19:17:13+07:00\",\n  \"updated_at\": \"2016-12-01T19:17:13+07:00\",\n  \"users\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/roles_apidoc.rb",
    "groupTitle": "Role"
  },
  {
    "type": "delete",
    "url": "/api/v1/roles/:id",
    "title": "Destroy a role",
    "version": "1.0.0",
    "name": "DestroyARole",
    "group": "Role",
    "description": "<p>ลบข้อมูลของ role โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>Token is received from previous request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>Token is received from authentication.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>Email of current user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization-Header-Example:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา role ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "NotFound-Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/roles_apidoc.rb",
    "groupTitle": "Role"
  },
  {
    "type": "get",
    "url": "/api/v1/roles/:id",
    "title": "Get a role",
    "version": "1.0.0",
    "name": "GetARole",
    "group": "Role",
    "description": "<p>ดูข้อมูลของ role ตาม id โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>Token is received from previous request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>Token is received from authentication.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>Email of current user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization-Header-Example:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ Role</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อของ Role</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง object</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่แก้ไขข้อมูลของ object</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "users",
            "description": "<p>user ทั้งหมดที่เป็น role นี้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"name\": \"member\",\n  \"created_at\": \"2016-11-30T21:18:55+07:00\",\n  \"updated_at\": \"2016-11-30T21:18:55+07:00\",\n  \"users\": [\n    {\n      \"id\": 2,\n      \"provider\": \"email\",\n      \"uid\": \"tim_berner@email.com\",\n      \"email\": \"tim_berner@email.com\",\n      \"name\": \"Tim Berner\",\n      \"image\": null,\n      \"birthday\": \"1990-02-19\",\n      \"gender\": \"male\",\n      \"phone_number\": \"034658471\",\n      \"created_at\": \"2016-11-30T21:18:55+07:00\",\n      \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา role ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "NotFound-Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/roles_apidoc.rb",
    "groupTitle": "Role"
  },
  {
    "type": "get",
    "url": "/api/v1/roles",
    "title": "Get all roles",
    "version": "1.0.0",
    "name": "GetRoles",
    "group": "Role",
    "description": "<p>ดูข้อมูลของ roles ทั้งหมด โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>Token is received from previous request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>Token is received from authentication.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>Email of current user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization-Header-Example:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ Role</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อของ Role</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง object</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่แก้ไขข้อมูลของ object</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "users",
            "description": "<p>user ทั้งหมดที่เป็น role นี้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n    \"id\": 1,\n    \"name\": \"member\",\n    \"created_at\": \"2016-11-30T21:18:55+07:00\",\n    \"updated_at\": \"2016-11-30T21:18:55+07:00\",\n    \"users\": [\n      {\n        \"id\": 2,\n        \"provider\": \"email\",\n        \"uid\": \"tim_berner@email.com\",\n        \"email\": \"tim_berner@email.com\",\n        \"name\": \"Tim Berner\",\n        \"image\": null,\n        \"birthday\": \"1990-02-19\",\n        \"gender\": \"male\",\n        \"phone_number\": \"034658471\",\n        \"created_at\": \"2016-11-30T21:18:55+07:00\",\n        \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n      },\n      {\n        \"id\": 3,\n        \"provider\": \"email\",\n        \"uid\": \"sam@email.com\",\n        \"email\": \"sam@email.com\",\n        \"name\": \"Sam Smith\",\n        \"image\": null,\n        \"birthday\": \"1987-06-20\",\n        \"gender\": \"male\",\n        \"phone_number\": \"0899999999\",\n        \"created_at\": \"2016-11-30T21:18:55+07:00\",\n        \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n      }\n    ]\n  },\n  {\n    \"id\": 2,\n    \"name\": \"producer\",\n    \"created_at\": \"2016-11-30T21:18:55+07:00\",\n    \"updated_at\": \"2016-11-30T21:18:55+07:00\",\n    \"users\": []\n  },\n  {\n    \"id\": 3,\n    \"name\": \"admin\",\n    \"created_at\": \"2016-11-30T21:18:55+07:00\",\n    \"updated_at\": \"2016-11-30T21:18:55+07:00\",\n    \"users\": [\n      {\n        \"id\": 1,\n        \"provider\": \"email\",\n        \"uid\": \"o_k_t@hotmail.com\",\n        \"email\": \"o_k_t@hotmail.com\",\n        \"name\": \"Tatchagon Koonkoei\",\n        \"image\": null,\n        \"birthday\": \"1993-06-02\",\n        \"gender\": \"male\",\n        \"phone_number\": \"0826810461\",\n        \"created_at\": \"2016-11-30T21:18:55+07:00\",\n        \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n      }\n    ]\n  }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/roles_apidoc.rb",
    "groupTitle": "Role"
  },
  {
    "type": "put",
    "url": "/api/v1/roles/:id",
    "title": "Update a role",
    "version": "1.0.0",
    "name": "UpdateARole",
    "group": "Role",
    "description": "<p>แก้ไขข้อมูลของ role โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>Token is received from previous request.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>Token is received from authentication.</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>Email of current user.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Authorization-Header-Example:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Role Param": [
          {
            "group": "Role Param",
            "type": "String",
            "optional": false,
            "field": "role[name]",
            "description": "<p>Name of the Role.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Role-Param-Example:",
          "content": "{\n  \"role\" : {\n    \"name\": \"member edit\",\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา role ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล role ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "NotFound-Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"name\": [\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 4,\n  \"name\": \"member edit\",\n  \"created_at\": \"2016-12-01T19:17:13+07:00\",\n  \"updated_at\": \"2016-12-01T19:18:33+07:00\",\n  \"users\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/roles_apidoc.rb",
    "groupTitle": "Role"
  },
  {
    "type": "post",
    "url": "/api/v1/users/",
    "title": "Create a user",
    "version": "1.0.0",
    "name": "CreateAUser",
    "group": "User",
    "description": "<p>สร้าง user ใหม่</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user[name]",
            "description": "<p>ชื่อนามสกุลของ user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user[email]",
            "description": "<p>email ของ user</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "user[birthday]",
            "description": "<p>วันเกิดของ user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "user[gender]",
            "description": "<p>เพศของ user มีค่าเป็น <code>male</code> หรือ <code>female</code> เท่านั้น</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "user[phone_number]",
            "description": "<p>เบอร์โทรศัพท์ของ user มีค่าเป็นตัวเลขจำนวน 9 หรือ 10 หลักเท่านั้น</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "user[image]",
            "description": "<p>ที่อยู่ของรูปภาพโปรไฟล์ของ user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user[password]",
            "description": "<p>รหัสผ่าน</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user[password_confirmation]",
            "description": "<p>ยืนยันรหัสผ่าน</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-User-Param:",
          "content": "{\n  \"user\" : {\n    \"name\": \"Sam Smith\",\n    \"email\": \"email@example.com\",\n    \"birthday\": \"02/06/1993\",\n    \"gender\": \"male\",\n    \"phone_number\": \"0826810461\",\n    \"password\": \"password\",\n    \"password_confirmation\": \"password\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่ม User ใหม่ได้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Validation-Error:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"password\": [\n    \"can't be blank\"\n  ],\n  \"email\": [\n    \"can't be blank\",\n    \"is not an email\"\n  ],\n  \"name\": [\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 201 Created\n{\n  \"id\": 3,\n  \"provider\": \"email\",\n  \"uid\": \"sam_smith@example.email.com\",\n  \"email\": \"sam_smith@example.email.com\",\n  \"name\": \"Sam Smith\",\n  \"image\": null,\n  \"birthday\": \"1993-06-02\",\n  \"gender\": \"male\",\n  \"phone_number\": \"0891234567\",\n  \"created_at\": \"2016-11-30T23:05:47+07:00\",\n  \"updated_at\": \"2016-11-30T23:05:47+07:00\",\n  \"role\": {\n    \"id\": 1,\n    \"name\": \"member\",\n    \"created_at\": \"2016-11-30T21:18:55+07:00\",\n    \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n  },\n  \"products\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/users_apidoc.rb",
    "groupTitle": "User"
  },
  {
    "type": "delete",
    "url": "/api/v1/users/:id",
    "title": "Destroy a user",
    "version": "1.0.0",
    "name": "DestroyAUser",
    "group": "User",
    "description": "<p>ลบข้อมูล user โดยต้องเข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin เท่านั้น</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา user ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 204 No Content",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/users_apidoc.rb",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/v1/users/:id",
    "title": "Get a user",
    "version": "1.0.0",
    "name": "GetAUser",
    "group": "User",
    "description": "<p>เรียกดูข้อมูลของ user ตาม id</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "provider",
            "description": "<p>ถ้า user สมัครใช้งานด้วย email ก็จะมีค่าเป็น <code>email</code> แต่ถ้ามีสมัครด้วย Facebook ก็จะมีค่าเป็น <code>facebook</code></p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>มีค่าเดียวกับ email ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อนามสกุลของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>ที่อยู่ของรูปภาพโปรไฟล์ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "birthday",
            "description": "<p>วันเกิดของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>เพศของ user มีค่าเป็น <code>male</code> หรือ <code>female</code> เท่านั้น</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone_number",
            "description": "<p>เบอร์โทรศัพท์ของ user มีค่าเป็นตัวเลขจำนวน 9 หรือ 10 หลักเท่านั้น</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง user</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่มีการแก้ไขข้อมูล user</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "role",
            "description": "<p>Role ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "products",
            "description": "<p>ข้อมูลสินค้าทั้งหมดของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 1,\n  \"provider\": \"email\",\n  \"uid\": \"o_k_t@hotmail.com\",\n  \"email\": \"o_k_t@hotmail.com\",\n  \"name\": \"Tatchagon Koonkoei\",\n  \"image\": null,\n  \"birthday\": \"1993-06-02\",\n  \"gender\": \"male\",\n  \"phone_number\": \"0826810461\",\n  \"created_at\": \"2016-11-30T21:18:55+07:00\",\n  \"updated_at\": \"2016-11-30T21:18:55+07:00\",\n  \"role\": {\n    \"id\": 3,\n    \"name\": \"admin\",\n    \"created_at\": \"2016-11-30T21:18:55+07:00\",\n    \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n  },\n  \"products\": [\n    {\n      \"id\": 1,\n      \"name\": \"Sky Coffee\",\n      \"description\": \"content\",\n      \"unit\": \"box\",\n      \"created_at\": \"2016-11-30T21:18:55+07:00\",\n      \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n    },\n    {\n      \"id\": 2,\n      \"name\": \"3D Nano\",\n      \"description\": \"content\",\n      \"unit\": \"can\",\n      \"created_at\": \"2016-11-30T21:18:55+07:00\",\n      \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n    },\n    {\n      \"id\": 3,\n      \"name\": \"AK-47\",\n      \"description\": \"content\",\n      \"unit\": \"gun barrel\",\n      \"created_at\": \"2016-11-30T21:18:55+07:00\",\n      \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา user ที่ตรงกับ <code>id</code> ได้</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/users_apidoc.rb",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/v1/users",
    "title": "Get all users",
    "version": "1.0.0",
    "name": "GetUsers",
    "group": "User",
    "description": "<p>เรียกดูข้อมูลของ user ทั้งหมด</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Id ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "provider",
            "description": "<p>ถ้า user สมัครใช้งานด้วย email ก็จะมีค่าเป็น <code>email</code> แต่ถ้ามีสมัครด้วย Facebook ก็จะมีค่าเป็น <code>facebook</code></p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>มีค่าเดียวกับ email ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>email ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>ชื่อนามสกุลของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "image",
            "description": "<p>ที่อยู่ของรูปภาพโปรไฟล์ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "birthday",
            "description": "<p>วันเกิดของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "gender",
            "description": "<p>เพศของ user มีค่าเป็น <code>male</code> หรือ <code>female</code> เท่านั้น</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "phone_number",
            "description": "<p>เบอร์โทรศัพท์ของ user มีค่าเป็นตัวเลขจำนวน 9 หรือ 10 หลักเท่านั้น</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>วันเวลาที่สร้าง user</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>วันเวลาล่าสุดที่มีการแก้ไขข้อมูล user</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "role",
            "description": "<p>Role ของ user</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "products",
            "description": "<p>ข้อมูลสินค้าทั้งหมดของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n[\n  {\n    \"id\": 1,\n    \"provider\": \"email\",\n    \"uid\": \"o_k_t@hotmail.com\",\n    \"email\": \"o_k_t@hotmail.com\",\n    \"name\": \"Tatchagon Koonkoei\",\n    \"image\": null,\n    \"birthday\": \"1993-06-02\",\n    \"gender\": \"male\",\n    \"phone_number\": \"0826810461\",\n    \"created_at\": \"2016-11-30T21:18:55+07:00\",\n    \"updated_at\": \"2016-11-30T21:18:55+07:00\",\n    \"role\": {\n      \"id\": 3,\n      \"name\": \"admin\",\n      \"created_at\": \"2016-11-30T21:18:55+07:00\",\n      \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n    },\n    \"products\": [\n      {\n        \"id\": 1,\n        \"name\": \"Sky Coffee\",\n        \"description\": \"content\",\n        \"unit\": \"box\",\n        \"created_at\": \"2016-11-30T21:18:55+07:00\",\n        \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n      },\n      {\n        \"id\": 2,\n        \"name\": \"3D Nano\",\n        \"description\": \"content\",\n        \"unit\": \"can\",\n        \"created_at\": \"2016-11-30T21:18:55+07:00\",\n        \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n      }\n    ]\n  },\n  {\n    \"id\": 2,\n    \"provider\": \"email\",\n    \"uid\": \"tim_berner@email.com\",\n    \"email\": \"tim_berner@email.com\",\n    \"name\": \"Tim Berner\",\n    \"image\": null,\n    \"birthday\": \"1990-02-19\",\n    \"gender\": \"male\",\n    \"phone_number\": \"034658471\",\n    \"created_at\": \"2016-11-30T21:18:55+07:00\",\n    \"updated_at\": \"2016-11-30T21:18:55+07:00\",\n    \"role\": {\n      \"id\": 1,\n      \"name\": \"member\",\n      \"created_at\": \"2016-11-30T21:18:55+07:00\",\n      \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n    },\n    \"products\": []\n  }\n]",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/users_apidoc.rb",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/api/v1/users/:id",
    "title": "Update a user",
    "version": "1.0.0",
    "name": "UpdateAUser",
    "group": "User",
    "description": "<p>แก้ไขข้อมูล user โดยจะต้องเข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin หรือผู้ใช้ที่เป็นเจ้าของข้อมูลที่ต้องการแก้ไข</p>",
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "access-token",
            "description": "<p>ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "client",
            "description": "<p>ส่งข้อมูล client Token ที่ได้รับจากการ sign in</p>"
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "uid",
            "description": "<p>ส่งข้อมูล email ของ user</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-Header:",
          "content": "{\n  \"access-token\": \"7iI0KfNDWrAiyIi3nL_ega\",\n  \"client\": \"8Q8-EauC_ioAegsRS2S74b\",\n  \"uid\": \"example@email.com\"\n}",
          "type": "json"
        }
      ]
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user[name]",
            "description": "<p>ชื่อนามสกุลของ user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "user[email]",
            "description": "<p>email ของ user</p>"
          },
          {
            "group": "Parameter",
            "type": "Date",
            "optional": true,
            "field": "user[birthday]",
            "description": "<p>วันเกิดของ user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "user[gender]",
            "description": "<p>เพศของ user มีค่าเป็น <code>male</code> หรือ <code>female</code> เท่านั้น</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "user[phone_number]",
            "description": "<p>เบอร์โทรศัพท์ของ user มีค่าเป็นตัวเลขจำนวน 9 หรือ 10 หลักเท่านั้น</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "user[image]",
            "description": "<p>ที่อยู่ของรูปภาพโปรไฟล์ของ user</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "user[password]",
            "description": "<p>รหัสผ่าน</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "user[password_confirmation]",
            "description": "<p>ยืนยันรหัสผ่าน</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "ตัวอย่าง-User-Param:",
          "content": "{\n  \"user\" : {\n    \"name\": \"Sam Smith\",\n    \"email\": \"email@example.com\",\n    \"birthday\": \"02/06/1993\",\n    \"gender\": \"male\",\n    \"phone_number\": \"0826810461\",\n    \"password\": \"password\",\n    \"password_confirmation\": \"password\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NotFound",
            "description": "<p>ไม่สามารถหา user ที่ตรงกับ <code>id</code> ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UnprocessableEntity",
            "description": "<p>ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล user ได้</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Forbidden",
            "description": "<p>เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin หรือไม่ได้เป็นเข้าของข้อมูลที่ต้องการแก้ไข</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 404 Not Found\n{\n  \"error\": \"Not found\"\n}",
          "type": "json"
        },
        {
          "title": "Validation-Error-Response:",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"password\": [\n    \"can't be blank\"\n  ],\n  \"email\": [\n    \"can't be blank\",\n    \"is not an email\"\n  ],\n  \"name\": [\n    \"can't be blank\"\n  ]\n}",
          "type": "json"
        },
        {
          "title": "Forbidden-Error-Response:",
          "content": "HTTP/1.1 403 Forbidden\n{\n  \"error\": \"Permission denied\",\n  \"status\": 403\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"id\": 3,\n  \"provider\": \"email\",\n  \"uid\": \"marry_jane@example.com\",\n  \"email\": \"marry_jane@example.com\",\n  \"name\": \"Marry Jane\",\n  \"image\": null,\n  \"birthday\": \"1989-05-16\",\n  \"gender\": \"famale\",\n  \"phone_number\": \"0826810461\",\n  \"created_at\": \"2016-11-30T23:05:47+07:00\",\n  \"updated_at\": \"2016-11-30T23:38:26+07:00\",\n  \"role\": {\n    \"id\": 1,\n    \"name\": \"member\",\n    \"created_at\": \"2016-11-30T21:18:55+07:00\",\n    \"updated_at\": \"2016-11-30T21:18:55+07:00\"\n  },\n  \"products\": []\n}",
          "type": "json"
        }
      ]
    },
    "filename": "app/apidoc/v1/users_apidoc.rb",
    "groupTitle": "User"
  }
] });
