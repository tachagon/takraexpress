require 'test_helper'

class UserTest < ActiveSupport::TestCase

	def setup
    @category = categories(:female_fashion)
		@user = User.new(
			email: 'example_user@email.com',
			password: 'password',
			name: 'Harry Potter',
			# birthday: '02/06/1993',
			# gender: 'male',
			# phone_number: '0826810461'
		)
	end

	test "user should be valid" do
		assert @user.valid?
	end

  # ===================================================
  # name
  # ===================================================

  test "name should be not too long" do
    @user.name = "a" * 256
    assert_not @user.valid?
  end

  test "name should be present" do
    @user.name = ""
    assert_not @user.valid?
  end

  # ===================================================
  # birthday
  # ===================================================

  # ===================================================
  # gender
  # ===================================================

  # test "gender should be male or female" do
  #   @user.gender = "male"
  #   assert @user.valid?

  #   @user.gender = "female"
  #   assert @user.valid?

  #   @user.gender = "anything"
  #   assert_not @user.valid?
  # end

  # test "gender should be present" do
  #   @user.gender = "     "
  #   assert_not @user.valid?
  # end

  # ===================================================
  # phone_number
  # ===================================================

  # test "phone_number should be format" do
  #   @user.phone_number = "1826810461"
  #   assert_not @user.valid?

  #   @user.phone_number = "082a810461"
  #   assert_not @user.valid?

  #   @user.phone_number = "082-6810461"
  #   assert_not @user.valid?

  #   @user.phone_number = "0826810461"
  #   assert @user.valid?

  #   @user.phone_number = "034658471"
  #   assert @user.valid?
  # end

  # test "phone_number length should be 9 to 10" do
  #   @user.phone_number = "0" * 8
  #   assert_not @user.valid?

  #   @user.phone_number = "0" * 11
  #   assert_not @user.valid?
  # end

  # ===================================================
  # role
  # ===================================================

  test "role should be present" do
    @user.role = nil
    assert_not @user.valid?
  end

  test "default role should be member" do
    @user.save
    @user.reload
    assert_equal @user.role, Role.member
  end

  test "can set user role in add user on rails console case" do
  	@user.role = Role.admin
  	@user.save
  	assert_equal(@user.reload.role, Role.admin)
  end

  # ===================================================
  # association
  # ===================================================

	test "associated products should be destroyed" do
		@user.save
		@user.products.create!(name: 'some product', category: @category)
		assert_difference "Product.count", -1 do
			@user.destroy
		end
	end

  # ===================================================
  # public function
  # ===================================================

  test "member? should be correct" do
    @user.role = Role.admin
    @user.save
    assert_equal(@user.reload.member?, false)

    @user.role = Role.member
    @user.save
    assert_equal(@user.reload.member?, true)

    @user.role = Role.producer
    @user.save
    assert_equal(@user.reload.member?, false)
  end

  test "producer? should be correct" do
    @user.role = Role.admin
    @user.save
    assert_equal(@user.reload.producer?, false)

    @user.role = Role.member
    @user.save
    assert_equal(@user.reload.producer?, false)

    @user.role = Role.producer
    @user.save
    assert_equal(@user.reload.producer?, true)
  end

  test "admin? should be correct" do
    @user.role = Role.admin
    @user.save
    assert_equal(@user.reload.admin?, true)

    @user.role = Role.member
    @user.save
    assert_equal(@user.reload.admin?, false)

    @user.role = Role.producer
    @user.save
    assert_equal(@user.reload.admin?, false)
  end

end
