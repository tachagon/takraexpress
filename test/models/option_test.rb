require 'test_helper'

class OptionTest < ActiveSupport::TestCase

	def setup
		@product = products(:one)
		@option = Option.new(
			name: 'option',
			price: 5.5,
			quantity: 100,
			# weight: 150,
			product: @product
		)
	end

	test "option should be valid" do
		assert @option.valid?
	end

	# ================================================
	# name
	# ================================================

	test "name should be too long" do
		@option.name = 'a' * 201
		assert_not @option.valid?
	end

	test "name should be present" do
		@option.name = ''
		assert_not @option.valid?
	end

	# ================================================
	# price
	# ================================================

	test "price should be number" do
		@option.price = 'some word'
		assert_not @option.valid?
	end

	test "price should be greater than or equal to 0" do
		@option.price = -0.1
		assert_not @option.valid?
	end

	test "price should be present" do
		@option.price = nil
		assert_not @option.valid?
	end

	# ================================================
	# quantity
	# ================================================

	test "quantity should be number" do
		@option.quantity = 'some word'
		assert_not @option.valid?
	end

	test "quantity should be integer" do
		@option.quantity = 1.2
		assert_not @option.valid?
	end

	test "quantity should be greater than or equal to 0" do
		@option.quantity = -1
		assert_not @option.valid?
	end

	test "quantity should be present" do
		@option.quantity = nil
		assert_not @option.valid?
	end

	# ================================================
	# weight
	# ================================================

	test "weight should be number" do
		@option.weight = 'some word'
		assert_not @option.valid?
	end

	test "weight should be greater than or equal to 0" do
		@option.weight = -0.1
		assert_not @option.valid?
	end

	# ================================================
	# product
	# ================================================

	test "product should be present" do
		@option.product = nil
		assert_not @option.valid?
	end

end
