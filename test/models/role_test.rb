require 'test_helper'

class RoleTest < ActiveSupport::TestCase

	def setup
		@role = Role.new(name: "role_name")
	end

	test "role should be valid" do
		assert @role.valid?
	end

	# ====================================================
	# name
	# ====================================================

	test "name should be present" do
		@role.name = ""
		assert_not @role.valid?
	end

	test "name should be uniq" do
		dup_role = @role.dup
		@role.save
		assert_not dup_role.valid?
	end

	test "name should be save in downcase" do
		name = "UPCASE"
		@role.name = name
		@role.save
		assert_equal(@role.reload.name, name.downcase)
	end

	# ====================================================
	# public function
	# ====================================================

	test "Role.member function should be return member role" do
		member = roles(:member)
		assert_equal(Role.member, member)
	end

	test "Role.producer function should be return producer role" do
		producer = roles(:producer)
		assert_equal(Role.producer, producer)
	end

	test "Role.admin function should be return admin role" do
		admin = roles(:admin)
		assert_equal(Role.admin, admin)
	end

	# ====================================================
	# association
	# ====================================================

	test "associated users should be destroyed" do
		@role.save
		@role.users.create!(
			email: 'user_role@examplemail.com',
			password: 'password',
			name: 'Adam Smith',
			birthday: '02/06/1993',
			gender: 'male',
			phone_number: '0826810461'
		)
		assert_difference "User.count", -1 do
			@role.destroy
		end
	end

end
