require 'test_helper'

class ProductTest < ActiveSupport::TestCase

	def setup
		@user = users(:kol)
		@category = categories(:female_fashion)
		@product = Product.new(
			name: "กระโปรง",
			user: @user,
			category: @category
		)
	end

	test "product should be valid" do
		assert @product.valid?
	end

	# ===================================================
	# call back
	# ===================================================

	test "unit should be set to default" do
		@product.unit = ''
		@product.save
		assert_equal(@product.reload.unit, 'หน่วย')
	end

	test "unit should not be default if user set unit" do
		unit = 'box'
		@product.unit = unit
		@product.save
		assert_equal(@product.reload.unit, unit)
	end

	# ===================================================
	# validation
	# ===================================================

	test "name should be present" do
		@product.name = ""
		assert_not @product.valid?
	end

	test "user should be present" do
		@product.user = nil
		assert_not @product.valid?
	end

	# ===================================================
	# association
	# ===================================================

	test "associated options should be destroyed" do
		@product.save
		@product.options.create!(
			name: 'option',
			price: 10.5,
			quantity: 20,
			weight: 300
		)
		assert_difference "Option.count", -1 do
			@product.destroy
		end
	end

end
