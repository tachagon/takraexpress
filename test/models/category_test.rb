require 'test_helper'

class CategoryTest < ActiveSupport::TestCase

	def setup
		@user = users(:kol)
		@category = Category.new(name: 'เสื้อผ้าแฟชั่นผู้หญิง')
	end

	test "category should be valid" do
		assert @category.valid?
	end

	# ========================================================
	# name
	# ========================================================

	test "name should be present" do
		@category.name = ""
		assert_not @category.valid?
	end

	# ========================================================
	# association
	# ========================================================

	test "association subcategories should be destroyed" do
		@category.save
		@category.subcategories.create!(name: 'เดรส')
		assert_difference "Category.count", -2 do
			@category.destroy
		end
	end

	test "association products should be destroyed" do
		@category.save
		@category.products.create!(name: 'product', user: @user)
		assert_difference "Product.count", -1 do
		  @category.destroy
		end
	end

end
