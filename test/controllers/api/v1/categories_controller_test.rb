require 'test_helper'

class Api::V1::CategoriesControllerTest < ActionDispatch::IntegrationTest

	def setup
		@admin = users(:kol)
		@member = users(:pasin)
		@category = categories(:dress)
	end

  # ===================================================================
  # index
  # ===================================================================

  test "should get index" do
    get api_v1_categories_url, as: :json
    assert_response :success
  end

  # ===================================================================
  # show
  # ===================================================================

  test "should set category before show category" do
    get api_v1_category_url(-1), as: :json
    assert_response :not_found
  end

  test "should show category" do
    get api_v1_category_url(@category), as: :json
    assert_response :success
  end

  # ===================================================================
  # create
  # ===================================================================

  test "should authenticate user before create category" do
    assert_no_difference('Category.count') do
      post api_v1_categories_url,
        params: { category:
          {
          	name: 'some category',
            description: 'some content',
            image: 'some path'
          }
        },
        as: :json
    end

    assert_response 401
  end

  test "should authenticate admin user before create category" do
  	res = @member.create_new_auth_token
    assert_no_difference('Category.count') do
      post api_v1_categories_url,
        params: { category:
          {
          	name: 'some category',
            description: 'some content',
            image: 'some path'
          }
        },
        as: :json,
        headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    end

    assert_response 403
  end

  test "should create category" do
  	res = @admin.create_new_auth_token
    assert_difference('Category.count') do
      post api_v1_categories_url,
        params: { category:
          {
          	name: 'some category',
            description: 'some content',
            image: 'some path'
          }
        },
        as: :json,
        headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    end

    assert_response 201
  end

  # ===================================================================
  # update
  # ===================================================================

  test "should authenticate user before update category" do
    patch api_v1_category_url(@category),
      params: { category:
        {
        	name: 'some category',
          description: 'some content',
          image: 'some path'
        }
      },
      as: :json
    assert_response 401
  end

  test "should authenticate admin user before update category" do
  	res = @member.create_new_auth_token
    patch api_v1_category_url(@category),
      params: { category:
        {
        	name: 'some category',
          description: 'some content',
          image: 'some path'
        }
      },
      as: :json,
      headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    assert_response 403
  end

  test "should set category before update category" do
  	res = @admin.create_new_auth_token
    patch api_v1_category_url(-1),
      params: { category:
        {
        	name: 'some category',
          description: 'some content',
          image: 'some path'
        }
      },
      as: :json,
      headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    assert_response 404
  end

  test "should update category" do
  	res = @admin.create_new_auth_token
    patch api_v1_category_url(@category),
      params: { category:
        {
        	name: 'some category',
          description: 'some content',
          image: 'some path'
        }
      },
      as: :json,
      headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    assert_response 200
  end

  # ===================================================================
  # destroy
  # ===================================================================

  test "should authenticate user before destroy category" do
    assert_no_difference "Category.count" do
    	delete api_v1_category_url(@category),
    	as: :json
    end

    assert_response 401
  end

  test "should authenticate admin user before destroy category" do
  	res = @member.create_new_auth_token
    assert_no_difference "Category.count" do
    	delete api_v1_category_url(@category),
    	as: :json,
    	headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    end

    assert_response 403
  end

  test "should set category before destroy category" do
  	res = @admin.create_new_auth_token
    assert_no_difference "Category.count" do
    	delete api_v1_category_url(-1),
    	as: :json,
    	headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    end

    assert_response 404
  end

  test "should destroy category" do
  	res = @admin.create_new_auth_token
    assert_difference "Category.count", -1 do
    	delete api_v1_category_url(@category),
    	as: :json,
    	headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    end

    assert_response 204
  end

end
