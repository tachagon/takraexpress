require 'test_helper'

class Api::V1::UsersControllerTest < ActionDispatch::IntegrationTest

	def setup
		@admin = users(:kol)
		@member = users(:pasin)
		@other_member = users(:marry)
	end

	# ===============================================================
	# update
	# ===============================================================

  test "no sign in user should not update user" do
    patch api_v1_user_url(@member),
      params: { user: { name: @member.name, email: @member.email } },
      as: :json
    assert_response 401
  end

  test "correct user should update user" do
    token = @member.create_new_auth_token
    patch api_v1_user_url(@member),
      params: { user: { name: @member.name, email: @member.email } },
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    assert_response 200
  end

  test "admin user should update user" do
    token = @admin.create_new_auth_token
    patch api_v1_user_url(@member),
      params: { user: { name: @member.name, email: @member.email } },
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    assert_response 200
  end

  test "incorrect user should not update user" do
    token = @other_member.create_new_auth_token
    patch api_v1_user_url(@member),
      params: { user: { name: @member.name, email: @member.email } },
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    assert_response 403
  end

	# ===============================================================
	# destroy
	# ===============================================================

  test "no sign in user should not destroy user" do
    assert_difference('User.count', 0) do
      delete api_v1_user_url(@member),
      as: :json
    end

    assert_response 401
  end

  test "admin user should destroy user" do
    token = @admin.create_new_auth_token
    assert_difference('User.count', -1) do
      delete api_v1_user_url(@member),
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    end

    assert_response 204
  end

  test "member user should not destroy user" do
    token = @member.create_new_auth_token
    assert_difference('User.count', 0) do
      delete api_v1_user_url(@other_member),
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    end

    assert_response 403
  end

end
