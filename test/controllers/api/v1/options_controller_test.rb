require 'test_helper'

class Api::V1::OptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin = users(:kol)
    @member = users(:pasin)
    @other_member = users(:marry)

    @product = products(:one)
    @product.user = @member
    @product.save

    @option = options(:option_one)
  end

  # =================================================================
  # index
  # =================================================================

  test "should get index" do
    get api_v1_options_url, as: :json
    assert_response :success
  end

  # =================================================================
  # show
  # =================================================================

  test "should set option before show option" do
    get api_v1_option_url(-1), as: :json
    assert_response :not_found
  end

  test "should show option" do
    get api_v1_option_url(@option), as: :json
    assert_response :success
  end

  # =================================================================
  # create
  # =================================================================

  test "should authenticate user before create option" do
    assert_no_difference('Option.count') do
      post api_v1_options_url,
        params: {
          option: {
            name: 'some option',
            price: 50.5,
            quantity: 100,
            weight: 450,
            product_id: @product.id
          }
        },
        as: :json
    end

    assert_response 401
  end

  test "should authenticate correct user before create option" do
    res = @other_member.create_new_auth_token
    assert_no_difference('Option.count') do
      post api_v1_options_url,
        params: {
          option: {
            name: 'some option',
            price: 50.5,
            quantity: 100,
            weight: 450,
            product_id: @product.id
          }
        },
        as: :json,
        headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    end

    assert_response 403
  end

  test "should authenticate admin before create option" do
    res = @admin.create_new_auth_token
    assert_difference('Option.count') do
      post api_v1_options_url,
        params: {
          option: {
            name: 'some option',
            price: 50.5,
            quantity: 100,
            weight: 450,
            product_id: @product.id
          }
        },
        as: :json,
        headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    end

    assert_response 201
  end

  test "should create option" do
    res = @member.create_new_auth_token
    assert_difference('Option.count') do
      post api_v1_options_url,
        params: {
          option: {
            name: 'some option',
            price: 50.5,
            quantity: 100,
            weight: 450,
            product_id: @product.id
          }
        },
        as: :json,
        headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    end

    assert_response 201
  end

  # =================================================================
  # update
  # =================================================================

  test "should authenticate user before update option" do
    patch api_v1_option_url(@option),
      params: {
        option: {
          name: 'update option',
          price: 430,
          quantity: 50,
          weight: 125,
          product_id: @product.id
        }
      },
      as: :json
    assert_response 401
  end

  test "should set option before update option" do
    res = @member.create_new_auth_token
    patch api_v1_option_url(-1),
      params: {
        option: {
          name: 'update option',
          price: 430,
          quantity: 50,
          weight: 125,
          product_id: @product.id
        }
      },
      as: :json,
      headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    assert_response 404
  end

  test "should authenticate correct user before update option" do
    res = @other_member.create_new_auth_token
    patch api_v1_option_url(@option),
      params: {
        option: {
          name: 'update option',
          price: 430,
          quantity: 50,
          weight: 125,
          product_id: @product.id
        }
      },
      as: :json,
      headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    assert_response 403
  end

  test "should authenticate admin user before update option" do
    res = @admin.create_new_auth_token
    patch api_v1_option_url(@option),
      params: {
        option: {
          name: 'update option',
          price: 430,
          quantity: 50,
          weight: 125,
          product_id: @product.id
        }
      },
      as: :json,
      headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    assert_response 200
  end

  test "should update option" do
    res = @member.create_new_auth_token
    patch api_v1_option_url(@option),
      params: {
        option: {
          name: 'update option',
          price: 430,
          quantity: 50,
          weight: 125,
          product_id: @product.id
        }
      },
      as: :json,
      headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    assert_response 200
  end

  # =================================================================
  # destroy
  # =================================================================

  test "should authenticate user before destroy option" do
    assert_no_difference('Option.count') do
      delete api_v1_option_url(@option),
        as: :json
    end

    assert_response 401
  end

  test "should set option before destroy option" do
    res = @member.create_new_auth_token
    assert_no_difference('Option.count') do
      delete api_v1_option_url(-1),
        as: :json,
        headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    end

    assert_response 404
  end

  test "should authenticate correct user before destroy option" do
    res = @other_member.create_new_auth_token
    assert_no_difference('Option.count') do
      delete api_v1_option_url(@option),
        as: :json,
        headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    end

    assert_response 403
  end

  test "should authenticate admin user before destroy option" do
    res = @admin.create_new_auth_token
    assert_difference('Option.count', -1) do
      delete api_v1_option_url(@option),
        as: :json,
        headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    end

    assert_response 204
  end

  test "should destroy option" do
    res = @member.create_new_auth_token
    assert_difference('Option.count', -1) do
      delete api_v1_option_url(@option),
        as: :json,
        headers: {'uid' => res['uid'], 'access-token' => res['access-token'], 'client' => res['client']}
    end

    assert_response 204
  end

end
