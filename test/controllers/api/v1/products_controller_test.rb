require 'test_helper'

class Api::V1::ProductsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:kol)
    @product = products(:one)
    @category = categories(:female_fashion)
  end

  # ===================================================================
  # index
  # ===================================================================

  test "should get index" do
    get api_v1_products_url, as: :json
    assert_response :success
  end

  # ===================================================================
  # show
  # ===================================================================

  test "should set product before show product" do
    get api_v1_product_url(-1), as: :json
    assert_response :not_found
  end

  test "should show product" do
    get api_v1_product_url(@product), as: :json
    assert_response :success
  end

  # ===================================================================
  # create
  # ===================================================================

  test "should authenticate user before create product" do
    assert_no_difference('Product.count') do
      post api_v1_products_url,
        params: { product:
          {
            description: @product.description,
            name: @product.name,
            unit: @product.unit,
            user_id: @product.user.id,
            category_id: @category.id
          }
        },
        as: :json
    end

    assert_response 401
  end

  test "should create product" do
    # sign in user
    post user_session_path, params: {email: @user.email, password: "password"}, as: :json
    # get response headers
    access_token = response.headers["access-token"]
    client = response.headers["client"]
    assert_difference('Product.count') do
      post api_v1_products_url,
        params: { product:
          {
            description: @product.description,
            name: @product.name,
            unit: @product.unit,
            user_id: @product.user.id,
            category_id: @category.id
          }
        },
        as: :json,
        headers: {"access-token" => access_token, "client" => client, "uid" => @user.email}
    end

    assert_response 201
  end

  # ===================================================================
  # update
  # ===================================================================

  test "should authenticate user before update product" do
    patch api_v1_product_url(@product),
      params: { product:
        {
          description: @product.description,
          name: @product.name,
          unit: @product.unit,
          user_id: @product.user.id,
          category_id: @category.id
        }
      },
      as: :json

    assert_response 401
  end

  test "should set product before update product" do
    res = @user.create_new_auth_token
    patch api_v1_product_url(-1),
      params: { product:
        {
          description: 'description',
          name: 'name',
          unit: 'unit',
          user_id: @user.id,
          category_id: @category.id
        }
      },
      as: :json,
      headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    assert_response 404
  end

  test "should update product" do
    res = @user.create_new_auth_token
    patch api_v1_product_url(@product),
      params: { product:
        {
          description: @product.description,
          name: @product.name,
          unit: @product.unit,
          user_id: @product.user.id,
          category_id: @category.id
        }
      },
      as: :json,
      headers: {"access-token" => res['access-token'], "client" => res['client'], "uid" => res['uid']}
    assert_response 200
  end

  # ===================================================================
  # destroy
  # ===================================================================

  test "should authenticate user before destroy product" do
    assert_no_difference('Product.count') do
      delete api_v1_product_url(@product),
      as: :json
    end

    assert_response 401
  end

  test "should set product before destroy product" do
    res = log_in_as @user
    assert_no_difference('Product.count') do
      delete api_v1_product_url(-1),
      as: :json,
      headers: {'access-token' => res.headers['access-token'], 'client' => res.headers['client'], 'uid' => res.headers['uid']}
    end

    assert_response 404
  end

  test "should destroy product" do
    res = log_in_as @user
    assert_difference('Product.count', -1) do
      delete api_v1_product_url(@product),
      as: :json,
      headers: {'access-token' => res.headers['access-token'], 'client' => res.headers['client'], 'uid' => res.headers['uid']}
    end

    assert_response 204
  end
end
