require 'test_helper'

class Api::V1::RolesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @role = roles(:member)
    @admin = users(:kol)
    @member = users(:pasin)
  end

  test "should get index" do
    token = @admin.create_new_auth_token
    get api_v1_roles_url,
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    assert_response :success
  end

  test "member user should not get index" do
    token = @member.create_new_auth_token
    get api_v1_roles_url,
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    assert_response :forbidden
  end

  test "should create role" do
    token = @admin.create_new_auth_token
    assert_difference('Role.count') do
      post api_v1_roles_url,
        params: { role: { name: "other_name" } },
        as: :json,
        headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    end

    assert_response 201
  end

  test "member user should not create role" do
    token = @member.create_new_auth_token
    assert_no_difference('Role.count') do
      post api_v1_roles_url,
        params: { role: { name: "Actor" } },
        as: :json,
        headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    end

    assert_response 403
  end

  test "should show role" do
    token = @admin.create_new_auth_token
    get api_v1_role_url(@role),
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    assert_response :success
  end

  test "member user should not show role" do
    token = @member.create_new_auth_token
    get api_v1_role_url(@role),
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    assert_response :forbidden
  end

  test "should update role" do
    token = @admin.create_new_auth_token
    patch api_v1_role_url(@role),
      params: { role: { name: @role.name } },
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    assert_response 200
  end

  test "member user should not update role" do
    token = @member.create_new_auth_token
    patch api_v1_role_url(@role),
      params: { role: { name: @role.name } },
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    assert_response 403
  end

  test "should destroy role" do
    token = @admin.create_new_auth_token
    assert_difference('Role.count', -1) do
      delete api_v1_role_url(@role),
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    end

    assert_response 204
  end

  test "member user should not destroy role" do
    token = @member.create_new_auth_token
    assert_no_difference('Role.count') do
      delete api_v1_role_url(@role),
      as: :json,
      headers: {'uid' => token['uid'], 'access-token' => token['access-token'], 'client' => token['client']}
    end

    assert_response 403
  end
end
