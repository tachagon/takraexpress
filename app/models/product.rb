class Product < ApplicationRecord
	before_save :set_default_unit

  belongs_to :user
  belongs_to :category
  has_many :options, dependent: :destroy

  validates :name, presence: true

  private

  	def set_default_unit
  		if self.unit == ''
  			self.unit = 'หน่วย'
  			self.save
  		end
  	end

end
