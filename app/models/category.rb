class Category < ApplicationRecord
	belongs_to :supercategory, class_name: 'Category', optional: true
	has_many :subcategories, class_name: 'Category', foreign_key: 'supercategory_id', dependent: :destroy
	has_many :products, dependent: :destroy

	validates :name, presence: true
end
