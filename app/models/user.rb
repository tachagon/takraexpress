class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable
  include DeviseTokenAuth::Concerns::User

  # ===================================================
  # association
  # ===================================================

  belongs_to :role

  has_many :products, dependent: :destroy

  # ===================================================
  # Call back
  # ===================================================

  after_initialize :set_default_role, if: :new_record?

  # ===================================================
  # validation
  # ===================================================

  validates :name,
    length: {maximum: 255},
    presence: true

  # validates :gender,
  #   inclusion: {in: %w(male female)},
  #   presence: true

  # VALID_PHONE_NUMBER_REGEX = /\A0\d{8,}\z/
  # validates :phone_number,
  #   format: {with: VALID_PHONE_NUMBER_REGEX},
  #   length: {in: 9..10}

  # ===================================================
  # public function
  # ===================================================

  def member?
    self.role == Role.member
  end

  def producer?
    self.role == Role.producer
  end

  def admin?
    self.role == Role.admin
  end

  # ===================================================
  # private function
  # ===================================================

  private

  	def set_default_role
  		self.role ||= Role.member
  	end

end
