class Option < ApplicationRecord
  belongs_to :product

  validates :name,
  	length: {maximum: 200},
  	presence: true

  validates :price,
  	numericality: {greater_than_or_equal_to: 0},
  	presence: true

  validates :quantity,
  	numericality: {only_integer: true, greater_than_or_equal_to: 0},
  	presence: true

  validates :weight,
  	numericality: {greater_than_or_equal_to: 0},
  	allow_nil: true

end
