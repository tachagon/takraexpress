class District < ApplicationRecord
  belongs_to :prefecture
  has_many :district_zipcodes
  has_many :zipcodes, through: :district_zipcodes
end
