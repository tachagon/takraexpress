class Role < ApplicationRecord
	before_save {name.downcase!}

	has_many :users, dependent: :destroy

	validates :name,
		presence: true,
		uniqueness: {case_sensitive: false}

	# ==============================================
	# class function
	# ==============================================

	def self.member
		Role.find_by(name: 'member')
	end

	def self.producer
		Role.find_by(name: 'producer')
	end

	def self.admin
		Role.find_by(name: 'admin')
	end

end
