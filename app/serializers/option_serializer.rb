class OptionSerializer < BaseSerializer
  attributes :id,
  	:name,
  	:price,
  	:quantity,
  	:weight,
  	:created_at,
  	:updated_at

  has_one :product
end
