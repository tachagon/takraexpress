class UserSerializer < BaseSerializer
  attributes :id,
  	:provider,
  	:uid,
  	:email,
  	:name,
  	:image,
  	:birthday,
  	:gender,
  	:phone_number,
  	:created_at,
  	:updated_at

  belongs_to :role
  has_many :products

end
