class DistrictSerializer < BaseSerializer
  attributes :id,
  	:name,
  	:latitude,
  	:longitude,
  	:created_at,
  	:updated_at

  belongs_to :prefecture
  has_many :zipcodes
end
