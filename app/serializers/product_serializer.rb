class ProductSerializer < BaseSerializer
	  attributes :id,
  	:name,
  	:description,
  	:unit,
  	:created_at,
  	:updated_at

  belongs_to :user
  belongs_to :category
  has_many :options

  def created_at
  	object.created_at.in_time_zone.iso8601 if object.created_at
  end

	def updated_at
		object.updated_at.in_time_zone.iso8601 if object.created_at
	end
end
