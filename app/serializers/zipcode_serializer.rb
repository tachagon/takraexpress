class ZipcodeSerializer < ActiveModel::Serializer
  attributes :id,
  	:code,
  	:created_at,
  	:updated_at

  has_many :districts
end
