class CategorySerializer < BaseSerializer
  attributes :id,
  	:name,
  	:description,
  	:image,
  	:created_at,
  	:updated_at

  	belongs_to :supercategory
  	has_many :subcategories
  	has_many :products
end
