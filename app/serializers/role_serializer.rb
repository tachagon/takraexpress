class RoleSerializer < BaseSerializer
  attributes :id,
  	:name,
  	:created_at,
  	:updated_at

  has_many :users
end
