class PrefectureSerializer < BaseSerializer
  attributes :id,
  	:name,
  	:created_at,
  	:updated_at

  belongs_to :province
  has_many :districts
end
