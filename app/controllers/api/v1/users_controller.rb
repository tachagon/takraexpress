class Api::V1::UsersController < Api::V1::BaseController
	before_action :authenticate_user!, only: [:update, :destroy]
	before_action :set_user, only: [:show, :update, :destroy]
	before_action :correct_or_admin_user, only: [:update]
	before_action :only_admin, only: [:destroy]

	def index
		@users = User.all

		render json: @users, each_serializer: UserSerializer
	end

	def show
		render json: @user, serializer: UserSerializer
	end

	def create
		@user = User.new(user_params)

		if @user.save
			render json: @user,
				serializer: UserSerializer,
				status: :created,
				location: @api_v1_user
		else
			render json: @user.errors,
			status: :unprocessable_entity
		end
	end

	def update
		if @user.update(user_params)
			render json: @user, serializer: UserSerializer
		else
			render json: @user.errors,
				status: :unprocessable_entity
		end
	end

	def destroy
		@user.destroy
	end

	private
		def correct_or_admin_user
			unless (@user == current_user) or (current_user.admin?)
        render json: {
          error: "Permission denied",
          status: 403
        }, status: :forbidden
			end
		end

		def only_admin
			unless current_user.admin?
        render json: {
          error: "Permission denied",
          status: 403
        }, status: :forbidden
			end
		end

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(
      	:name,
      	:email,
      	:birthday,
      	:gender,
      	:phone_number,
      	:image,
      	:password,
      	:password_confirmation
      )
    end

end
