class Api::V1::CategoriesController < Api::V1::BaseController
	before_action :authenticate_user!, only: [:create, :update, :destroy]
	before_action :only_admin, only: [:create, :update, :destroy]
	before_action :set_category, only: [:show, :update, :destroy]

	def index
		@categories = Category.all
		render json: @categories
	end

	def show
		render json: @category
	end

  def create
    @category = Category.new(category_params)

    if @category.save
      render json: @category, status: :created, location: @api_v1_category
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  def update
    if @category.update(category_params)
      render json: @category
    else
      render json: @category.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @category.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def category_params
      params.require(:category).permit(:name, :description, :image, :supercategory_id)
    end

    def only_admin
      unless current_user.admin?
        render json: {
          error: "Permission denied",
          status: 403
        }, status: :forbidden
      end
    end

end
