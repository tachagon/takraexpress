class Api::V1::ZipcodesController < Api::V1::BaseController

	def index
		@zipcodes = Zipcode.all
		render json: @zipcodes
	end

	def show
		@zipcode = Zipcode.find(params[:id])
		render json: @zipcode
	end

end
