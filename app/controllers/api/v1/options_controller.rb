class Api::V1::OptionsController < Api::V1::BaseController
  before_action :authenticate_user!, only: [:create, :update, :destroy]
  before_action :set_option, only: [:show, :update, :destroy]
  before_action :correct_or_admin_user, only: [:create, :update, :destroy]

  # GET /options.json
  def index
    @options = Option.all
    render json: @options
  end

  # GET /options/1.json
  def show
    render json: @option
  end

  # POST /options.json
  def create
    @option = Option.new(option_params)

    if @option.save
      render json: @option, status: :created, location: @api_v1_option
    else
      render json: @option.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /options/1.json
  def update
    if @option.update(option_params)
      render json: @option
    else
      render json: @option.errors, status: :unprocessable_entity
    end
  end

  # DELETE /options/1.json
  def destroy
    @option.destroy
  end

  private
    def correct_or_admin_user
      product_id = params[:option][:product_id] || @option.product.id
      product = Product.find_by_id(product_id)
      unless current_user == product.user or current_user.admin?
        render json: {
          error: "Permission denied",
          status: 403
        }, status: :forbidden
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_option
      @option = Option.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def option_params
      params.require(:option).permit(:name, :price, :quantity, :weight, :product_id)
    end
end
