class Api::V1::PrefecturesController < Api::V1::BaseController

	def index
		@prefectures = Prefecture.all
		render json: @prefectures
	end

	def show
		@prefecture = Prefecture.find(params[:id])
		render json: @prefecture
	end

end
