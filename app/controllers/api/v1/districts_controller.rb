class Api::V1::DistrictsController < Api::V1::BaseController

	def index
		@districts = District.all
		render json: @districts
	end

	def show
		@district = District.find(params[:id])
		render json: @district
	end

end
