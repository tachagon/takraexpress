# ============================= sign in ===========================
=begin

@api {post} /auth/sign_in Sign in
@apiVersion 1.0.0
@apiName SignIn
@apiGroup Auth

@apiDescription sign in เพื่อขอสิทธิ์ในการเข้าถึง resources ของระบบ

@apiParam {String}  email       email ของ user ที่ต้องการเข้าสู่ระบบ
@apiParam {String}  password    รหัสผ่านของ user ที่ต้องการเข้าสู่ระบบ

@apiParamExample {json} ตัวอย่าง-Param:
  {
    "email": "email@example.com",
    "password": "password"
  }

@apiError Unauthorized ข้อมูลที่ส่งมาไม่ถูกต้องจึงไม่สามารถเข้าสู่ระบบได้

@apiErrorExample ตัวอย่าง-Unauthorized-Error:
  HTTP/1.1 401 Unauthorized
  {
    "errors": [
      "Invalid login credentials. Please try again."
    ]
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "data": {
      "id": 1,
      "email": "o_k_t@hotmail.com",
      "provider": "email",
      "role_id": 3,
      "name": "Tatchagon Koonkoei",
      "uid": "o_k_t@hotmail.com",
      "nickname": null,
      "image": null,
      "birthday": "1993-06-02",
      "gender": "male",
      "phone_number": "0826810461"
    }
  }

=end

# ============================= sign out ===========================
=begin

@api {delete} /auth/sign_out Sign out
@apiVersion 1.0.0
@apiName SignOut
@apiGroup Auth

@apiDescription sign out เพื่อทำให้ user ออกจากการใช้งานระบบ

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiError NotFound ไม่สามารถหา user ได้เพราะผู้ใช้ยังไม่ได้เข้าสู่ระบบ หรือข้อมูลใน headers ไม่ถูกต้อง

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "errors": [
      "User was not found or was not logged in."
    ]
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "success": true
  }

=end