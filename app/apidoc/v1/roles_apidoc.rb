# ============================= index ===========================
=begin
@api {get} /api/v1/roles Get all roles
@apiVersion 1.0.0
@apiName GetRoles
@apiGroup Role

@apiDescription ดูข้อมูลของ roles ทั้งหมด โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน

@apiHeader (Header) {String} access-token  Token is received from previous request.
@apiHeader (Header) {String} client  Token is received from authentication.
@apiHeader (Header) {String} uid  Email of current user.

@apiHeaderExample {json} Authorization-Header-Example:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiSuccess {Integer}   id          Id ของ Role
@apiSuccess {String}    name        ชื่อของ Role
@apiSuccess {Datetime}  created_at  วันเวลาที่สร้าง object
@apiSuccess {Datetime}  updated_at  วันเวลาล่าสุดที่แก้ไขข้อมูลของ object
@apiSuccess {Object[]}  users       user ทั้งหมดที่เป็น role นี้

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  [
    {
      "id": 1,
      "name": "member",
      "created_at": "2016-11-30T21:18:55+07:00",
      "updated_at": "2016-11-30T21:18:55+07:00",
      "users": [
        {
          "id": 2,
          "provider": "email",
          "uid": "tim_berner@email.com",
          "email": "tim_berner@email.com",
          "name": "Tim Berner",
          "image": null,
          "birthday": "1990-02-19",
          "gender": "male",
          "phone_number": "034658471",
          "created_at": "2016-11-30T21:18:55+07:00",
          "updated_at": "2016-11-30T21:18:55+07:00"
        },
        {
          "id": 3,
          "provider": "email",
          "uid": "sam@email.com",
          "email": "sam@email.com",
          "name": "Sam Smith",
          "image": null,
          "birthday": "1987-06-20",
          "gender": "male",
          "phone_number": "0899999999",
          "created_at": "2016-11-30T21:18:55+07:00",
          "updated_at": "2016-11-30T21:18:55+07:00"
        }
      ]
    },
    {
      "id": 2,
      "name": "producer",
      "created_at": "2016-11-30T21:18:55+07:00",
      "updated_at": "2016-11-30T21:18:55+07:00",
      "users": []
    },
    {
      "id": 3,
      "name": "admin",
      "created_at": "2016-11-30T21:18:55+07:00",
      "updated_at": "2016-11-30T21:18:55+07:00",
      "users": [
        {
          "id": 1,
          "provider": "email",
          "uid": "o_k_t@hotmail.com",
          "email": "o_k_t@hotmail.com",
          "name": "Tatchagon Koonkoei",
          "image": null,
          "birthday": "1993-06-02",
          "gender": "male",
          "phone_number": "0826810461",
          "created_at": "2016-11-30T21:18:55+07:00",
          "updated_at": "2016-11-30T21:18:55+07:00"
        }
      ]
    }
  ]

@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

=end

# ============================= show ===========================
=begin

@api {get} /api/v1/roles/:id Get a role
@apiVersion 1.0.0
@apiName GetARole
@apiGroup Role

@apiDescription ดูข้อมูลของ role ตาม id โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน

@apiHeader (Header) {String} access-token  Token is received from previous request.
@apiHeader (Header) {String} client  Token is received from authentication.
@apiHeader (Header) {String} uid  Email of current user.

@apiHeaderExample {json} Authorization-Header-Example:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiSuccess {Integer}   id          Id ของ Role
@apiSuccess {String}    name        ชื่อของ Role
@apiSuccess {Datetime}  created_at  วันเวลาที่สร้าง object
@apiSuccess {Datetime}  updated_at  วันเวลาล่าสุดที่แก้ไขข้อมูลของ object
@apiSuccess {Object[]}  users       user ทั้งหมดที่เป็น role นี้

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 1,
    "name": "member",
    "created_at": "2016-11-30T21:18:55+07:00",
    "updated_at": "2016-11-30T21:18:55+07:00",
    "users": [
      {
        "id": 2,
        "provider": "email",
        "uid": "tim_berner@email.com",
        "email": "tim_berner@email.com",
        "name": "Tim Berner",
        "image": null,
        "birthday": "1990-02-19",
        "gender": "male",
        "phone_number": "034658471",
        "created_at": "2016-11-30T21:18:55+07:00",
        "updated_at": "2016-11-30T21:18:55+07:00"
      }
    ]
  }

@apiError NotFound ไม่สามารถหา role ที่ตรงกับ <code>id</code> ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin

@apiErrorExample NotFound-Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

=end

# ============================= create ===========================
=begin

@api {post} /api/v1/roles/ Create a role
@apiVersion 1.0.0
@apiName CreateARole
@apiGroup Role

@apiDescription เพิ่ม role ใหม่โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน

@apiHeader (Header) {String} access-token  Token is received from previous request.
@apiHeader (Header) {String} client  Token is received from authentication.
@apiHeader (Header) {String} uid  Email of current user.

@apiHeaderExample {json} Authorization-Header-Example:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiParam (Role Param) {String} role[name]  Name of the Role.

@apiParamExample {json} Role-Param-Example:
  {
    "role" : {
      "name": "member",
    }
  }

@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่มข้อมูล role ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin

@apiErrorExample Validation-Error-Response:
  HTTP/1.1 422 Unprocessable Entity
  {
    "name": [
      "can't be blank"
    ]
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 201 Created
  {
    "id": 4,
    "name": "actor",
    "created_at": "2016-12-01T19:17:13+07:00",
    "updated_at": "2016-12-01T19:17:13+07:00",
    "users": []
  }

=end

# ============================= update ===========================
=begin

@api {put} /api/v1/roles/:id Update a role
@apiVersion 1.0.0
@apiName UpdateARole
@apiGroup Role

@apiDescription แก้ไขข้อมูลของ role โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน

@apiHeader (Header) {String} access-token  Token is received from previous request.
@apiHeader (Header) {String} client  Token is received from authentication.
@apiHeader (Header) {String} uid  Email of current user.

@apiHeaderExample {json} Authorization-Header-Example:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiParam (Role Param) {String} role[name]  Name of the Role.

@apiParamExample {json} Role-Param-Example:
  {
    "role" : {
      "name": "member edit",
    }
  }

@apiError NotFound ไม่สามารถหา role ที่ตรงกับ <code>id</code> ได้
@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล role ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin

@apiErrorExample NotFound-Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Validation-Error-Response:
  HTTP/1.1 422 Unprocessable Entity
  {
    "name": [
      "can't be blank"
    ]
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 4,
    "name": "member edit",
    "created_at": "2016-12-01T19:17:13+07:00",
    "updated_at": "2016-12-01T19:18:33+07:00",
    "users": []
  }

=end

# ============================= destroy ===========================
=begin

@api {delete} /api/v1/roles/:id Destroy a role
@apiVersion 1.0.0
@apiName DestroyARole
@apiGroup Role

@apiDescription ลบข้อมูลของ role โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อน

@apiHeader (Header) {String} access-token  Token is received from previous request.
@apiHeader (Header) {String} client  Token is received from authentication.
@apiHeader (Header) {String} uid  Email of current user.

@apiHeaderExample {json} Authorization-Header-Example:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiError NotFound ไม่สามารถหา role ที่ตรงกับ <code>id</code> ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin

@apiErrorExample NotFound-Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 204 No Content

=end
