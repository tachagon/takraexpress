# ============================= index ===========================
=begin
@api {get} /api/v1/categories Get all categories
@apiVersion 1.0.0
@apiName GetCategories
@apiGroup Category

@apiDescription เรียกดูข้อมูลของประเภทสินค้าทั้งหมด

@apiSuccess {Integer}   id            Id ของ Category
@apiSuccess {String}    name          ชื่อของ Category
@apiSuccess {String}    description   คำอธิบายของ Category
@apiSuccess {String}    image         ที่อยู่ภาพของ Category
@apiSuccess {Datetime}  created_at    วันเวลาที่สร้าง Category
@apiSuccess {Datetime}  updated_at    วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Category
@apiSuccess {Object}    supercategory Object ของ parent category
@apiSuccess {Object[]}  subcategories Object Array ของ child category
@apiSuccess {Object[]}  products      Object Array ของ product ทั้งหมดใน category นั้น

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
[
  {
    "id": 1,
    "name": "อิเล็กทรอนิกส์",
    "description": "",
    "image": "",
    "created_at": "2016-12-03T22:52:25+07:00",
    "updated_at": "2016-12-03T22:52:25+07:00",
    "supercategory": null,
    "subcategories": [
      {
        "id": 2,
        "name": "โทรศัพท์มือถือ & แท็บเล็ต",
        "description": "",
        "image": "",
        "created_at": "2016-12-03T22:52:25+07:00",
        "updated_at": "2016-12-03T22:52:25+07:00"
      }
    ],
    "products": [
      {
        "id": 1,
        "name": "Sky Coffee",
        "description": "content",
        "unit": "box",
        "created_at": "2016-12-03T22:52:26+07:00",
        "updated_at": "2016-12-03T22:52:26+07:00"
      }
    ]
  },
  {
    "id": 2,
    "name": "โทรศัพท์มือถือ & แท็บเล็ต",
    "description": "",
    "image": "",
    "created_at": "2016-12-03T22:52:25+07:00",
    "updated_at": "2016-12-03T22:52:25+07:00",
    "supercategory": {
      "id": 1,
      "name": "อิเล็กทรอนิกส์",
      "description": "",
      "image": "",
      "created_at": "2016-12-03T22:52:25+07:00",
      "updated_at": "2016-12-03T22:52:25+07:00"
    },
    "subcategories": [
      {
        "id": 3,
        "name": "โทรศัพท์มือถือ",
        "description": "",
        "image": "",
        "created_at": "2016-12-03T22:52:25+07:00",
        "updated_at": "2016-12-03T22:52:25+07:00"
      },
      {
        "id": 4,
        "name": "แท็บเล็ต",
        "description": "",
        "image": "",
        "created_at": "2016-12-03T22:52:25+07:00",
        "updated_at": "2016-12-03T22:52:25+07:00"
      },
      {
        "id": 5,
        "name": "อุปกรณ์เสริมโทรศัพท์มือถือ",
        "description": "",
        "image": "",
        "created_at": "2016-12-03T22:52:25+07:00",
        "updated_at": "2016-12-03T22:52:25+07:00"
      }
    ],
    "products": [
      {
        "id": 2,
        "name": "3D Nano",
        "description": "content",
        "unit": "can",
        "created_at": "2016-12-03T22:52:26+07:00",
        "updated_at": "2016-12-03T22:52:26+07:00"
      }
    ]
  },
  {
    "id": 3,
    "name": "โทรศัพท์มือถือ",
    "description": "",
    "image": "",
    "created_at": "2016-12-03T22:52:25+07:00",
    "updated_at": "2016-12-03T22:52:25+07:00",
    "supercategory": {
      "id": 2,
      "name": "โทรศัพท์มือถือ & แท็บเล็ต",
      "description": "",
      "image": "",
      "created_at": "2016-12-03T22:52:25+07:00",
      "updated_at": "2016-12-03T22:52:25+07:00"
    },
    "subcategories": [],
    "products": []
  },
  {
    "id": 4,
    "name": "แท็บเล็ต",
    "description": "",
    "image": "",
    "created_at": "2016-12-03T22:52:25+07:00",
    "updated_at": "2016-12-03T22:52:25+07:00",
    "supercategory": {
      "id": 2,
      "name": "โทรศัพท์มือถือ & แท็บเล็ต",
      "description": "",
      "image": "",
      "created_at": "2016-12-03T22:52:25+07:00",
      "updated_at": "2016-12-03T22:52:25+07:00"
    },
    "subcategories": [],
    "products": []
  },
  {
    "id": 5,
    "name": "อุปกรณ์เสริมโทรศัพท์มือถือ",
    "description": "",
    "image": "",
    "created_at": "2016-12-03T22:52:25+07:00",
    "updated_at": "2016-12-03T22:52:25+07:00",
    "supercategory": {
      "id": 2,
      "name": "โทรศัพท์มือถือ & แท็บเล็ต",
      "description": "",
      "image": "",
      "created_at": "2016-12-03T22:52:25+07:00",
      "updated_at": "2016-12-03T22:52:25+07:00"
    },
    "subcategories": [
      {
        "id": 6,
        "name": "แบตเตอรีสำรอง",
        "description": "",
        "image": "",
        "created_at": "2016-12-03T22:52:25+07:00",
        "updated_at": "2016-12-03T22:52:25+07:00"
      },
      {
        "id": 7,
        "name": "แบตเตอรี และอุปกรณ์ชาร์จไฟ",
        "description": "",
        "image": "",
        "created_at": "2016-12-03T22:52:25+07:00",
        "updated_at": "2016-12-03T22:52:25+07:00"
      }
    ],
    "products": []
  },
  {
    "id": 6,
    "name": "แบตเตอรีสำรอง",
    "description": "",
    "image": "",
    "created_at": "2016-12-03T22:52:25+07:00",
    "updated_at": "2016-12-03T22:52:25+07:00",
    "supercategory": {
      "id": 5,
      "name": "อุปกรณ์เสริมโทรศัพท์มือถือ",
      "description": "",
      "image": "",
      "created_at": "2016-12-03T22:52:25+07:00",
      "updated_at": "2016-12-03T22:52:25+07:00"
    },
    "subcategories": [],
    "products": []
  },
  {
    "id": 7,
    "name": "แบตเตอรี และอุปกรณ์ชาร์จไฟ",
    "description": "",
    "image": "",
    "created_at": "2016-12-03T22:52:25+07:00",
    "updated_at": "2016-12-03T22:52:25+07:00",
    "supercategory": {
      "id": 5,
      "name": "อุปกรณ์เสริมโทรศัพท์มือถือ",
      "description": "",
      "image": "",
      "created_at": "2016-12-03T22:52:25+07:00",
      "updated_at": "2016-12-03T22:52:25+07:00"
    },
    "subcategories": [],
    "products": []
  }
]

=end

# ============================= show ===========================
=begin

@api {get} /api/v1/categories/:id Get a category
@apiVersion 1.0.0
@apiName GetACategory
@apiGroup Category

@apiDescription เรียกดูข้อมูลของ category ตาม id

@apiSuccess {Integer}   id            Id ของ Category
@apiSuccess {String}    name          ชื่อของ Category
@apiSuccess {String}    description   คำอธิบายของ Category
@apiSuccess {String}    image         ที่อยู่ภาพของ Category
@apiSuccess {Datetime}  created_at    วันเวลาที่สร้าง Category
@apiSuccess {Datetime}  updated_at    วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Category
@apiSuccess {Object}    supercategory Object ของ parent category
@apiSuccess {Object[]}  subcategories Object Array ของ child category
@apiSuccess {Object[]}  products      Object Array ของ product ทั้งหมดใน category นั้น

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 1,
    "name": "อิเล็กทรอนิกส์",
    "description": "",
    "image": "",
    "created_at": "2016-12-03T22:52:25+07:00",
    "updated_at": "2016-12-03T22:52:25+07:00",
    "supercategory": null,
    "subcategories": [
      {
        "id": 2,
        "name": "โทรศัพท์มือถือ & แท็บเล็ต",
        "description": "",
        "image": "",
        "created_at": "2016-12-03T22:52:25+07:00",
        "updated_at": "2016-12-03T22:52:25+07:00"
      }
    ],
    "products": [
      {
        "id": 1,
        "name": "Sky Coffee",
        "description": "content",
        "unit": "box",
        "created_at": "2016-12-03T22:52:26+07:00",
        "updated_at": "2016-12-03T22:52:26+07:00"
      }
    ]
  }

@apiError NotFound ไม่สามารถหา category ที่ตรงกับ <code>id</code> ได้

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

=end

# ============================= create ===========================
=begin

@api {post} /api/v1/categories/ Create a Category
@apiVersion 1.0.0
@apiName CreateACategory
@apiGroup Category

@apiDescription สร้าง category ใหม่ โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiParam (Category Param) {String}  category[name]                 ชื่อของ category
@apiParam (Category Param) {String}  [category[description]]        คำอธิบายของ category
@apiParam (Category Param) {String}  [category[image]]              ที่อยู่ภาพของ category
@apiParam (Category Param) {Integer} [category[supercategory_id]]   id ของ parent category

@apiParamExample {json} ตัวอย่าง-Product-Param:
  {
    "category" : {
      "name": "some category",
      "description": "some content",
      "image": "some where",
      "supercategory_id": 1
    }
  }

@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่ม Category ใหม่ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin

@apiErrorExample ตัวอย่าง-Validation-Error:
  HTTP/1.1 422 Unprocessable Entity
  {
    "name": [
      "can't be blank"
    ]
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 201 Created
  {
    "id": 10,
    "name": "some category",
    "description": "some content",
    "image": "some where",
    "created_at": "2016-12-03T23:18:30+07:00",
    "updated_at": "2016-12-03T23:18:30+07:00",
    "supercategory": {
      "id": 1,
      "name": "อิเล็กทรอนิกส์",
      "description": "",
      "image": "",
      "created_at": "2016-12-03T22:52:25+07:00",
      "updated_at": "2016-12-03T22:52:25+07:00"
    },
    "subcategories": [],
    "products": []
  }

=end

# ============================= update ===========================
=begin

@api {put} /api/v1/categories/:id Update a category
@apiVersion 1.0.0
@apiName UpdateACategory
@apiGroup Category

@apiDescription แก้ไขข้อมูล category โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiParam (Category Param) {String}  category[name]                 ชื่อของ category
@apiParam (Category Param) {String}  [category[description]]        คำอธิบายของ category
@apiParam (Category Param) {String}  [category[image]]              ที่อยู่ภาพของ category
@apiParam (Category Param) {Integer} [category[supercategory_id]]   id ของ parent category

@apiParamExample {json} ตัวอย่าง-Product-Param:
  {
    "category" : {
      "name": "some category",
      "description": "some content",
      "image": "some where",
      "supercategory_id": 1
    }
  }

@apiError NotFound ไม่สามารถหา category ที่ตรงกับ <code>id</code> ได้
@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล Category ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Validation-Error-Response:
  HTTP/1.1 422 Unprocessable Entity
  {
    "name": [
      "can't be blank"
    ]
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 10,
    "name": "edit category",
    "description": "des bla bla bla",
    "image": "img some where",
    "created_at": "2016-12-03T23:18:30+07:00",
    "updated_at": "2016-12-03T23:42:49+07:00",
    "supercategory": null,
    "subcategories": [],
    "products": []
  }

=end

# ============================= destroy ===========================
=begin

@api {delete} /api/v1/categories/:id Destroy a category
@apiVersion 1.0.0
@apiName DestroyACategory
@apiGroup Category

@apiDescription ลบข้อมูล category โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiError NotFound ไม่สามารถหา category ที่ตรงกับ <code>id</code> ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 204 No Content

=end