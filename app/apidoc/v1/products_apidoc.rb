# ============================= index ===========================
=begin
@api {get} /api/v1/products Get all products
@apiVersion 1.0.0
@apiName GetProducts
@apiGroup Product

@apiDescription เรียกดูข้อมูลของ product ทั้งหมด

@apiSuccess {Integer}   id            Id ของ Product
@apiSuccess {String}    name          ชื่อของ Product
@apiSuccess {String}    description   คำอธิบายของ Product
@apiSuccess {String}    unit          ชื่อหน่วยของ Product เช่น กล่อง, กระป๋อง
@apiSuccess {Datetime}  created_at    วันเวลาที่สร้าง Product
@apiSuccess {Datetime}  updated_at    วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Product
@apiSuccess {Object}    user          Object ของ user ที่เป็นเจ้าของ Product
@apiSuccess {Object}    category      Object ของ category คือประเภทของสินค้า
@apiSuccess {Object[]}  options       Object Array ของตัวเลือกสินค้า

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
[
  {
    "id": 1,
    "name": "Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook",
    "description": "content",
    "unit": "ชิ้น",
    "created_at": "2016-12-04T14:36:17+07:00",
    "updated_at": "2016-12-04T14:36:17+07:00",
    "user": {
      "id": 1,
      "provider": "email",
      "uid": "o_k_t@hotmail.com",
      "email": "o_k_t@hotmail.com",
      "name": "Tatchagon Koonkoei",
      "image": null,
      "birthday": "1993-06-02",
      "gender": "male",
      "phone_number": "0826810461",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "category": {
      "id": 1,
      "name": "อิเล็กทรอนิกส์",
      "description": "",
      "image": "",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "options": [
      {
        "id": 1,
        "name": "สีขาว",
        "price": 650,
        "quantity": 10,
        "weight": 500,
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      },
      {
        "id": 2,
        "name": "สีดำ",
        "price": 650,
        "quantity": 13,
        "weight": 500,
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    ]
  },
  {
    "id": 2,
    "name": "Samsung Galaxy J7 Prime",
    "description": "content",
    "unit": "เครื่อง",
    "created_at": "2016-12-04T14:36:17+07:00",
    "updated_at": "2016-12-04T14:36:17+07:00",
    "user": {
      "id": 3,
      "provider": "email",
      "uid": "leonado_davinci@email.com",
      "email": "leonado_davinci@email.com",
      "name": "Leonado Davinci",
      "image": null,
      "birthday": "1889-10-01",
      "gender": "male",
      "phone_number": "034658471",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "category": {
      "id": 2,
      "name": "โทรศัพท์มือถือ & แท็บเล็ต",
      "description": "",
      "image": "",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "options": [
      {
        "id": 3,
        "name": "White Gold",
        "price": 8250,
        "quantity": 20,
        "weight": 167,
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      },
      {
        "id": 4,
        "name": "Black",
        "price": 8250,
        "quantity": 12,
        "weight": 167,
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      },
      {
        "id": 5,
        "name": "Pink Gold",
        "price": 8250,
        "quantity": 15,
        "weight": 167,
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    ]
  },
  {
    "id": 3,
    "name": "เดรสยาวลายริ้ว คอกลม แขนสั้น",
    "description": "content",
    "unit": "ตัว",
    "created_at": "2016-12-04T14:36:17+07:00",
    "updated_at": "2016-12-04T14:36:17+07:00",
    "user": {
      "id": 1,
      "provider": "email",
      "uid": "o_k_t@hotmail.com",
      "email": "o_k_t@hotmail.com",
      "name": "Tatchagon Koonkoei",
      "image": null,
      "birthday": "1993-06-02",
      "gender": "male",
      "phone_number": "0826810461",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "category": {
      "id": 9,
      "name": "แฟชั่นสุภาพสตรี",
      "description": "",
      "image": "",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "options": [
      {
        "id": 6,
        "name": "size M",
        "price": 199,
        "quantity": 10,
        "weight": null,
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      },
      {
        "id": 7,
        "name": "size L",
        "price": 199,
        "quantity": 8,
        "weight": null,
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    ]
  }
]

=end

# ============================= show ===========================
=begin

@api {get} /api/v1/products/:id Get a product
@apiVersion 1.0.0
@apiName GetAProduct
@apiGroup Product

@apiDescription เรียกดูข้อมูลของ product ตาม id

@apiSuccess {Integer}   id            Id ของ Product
@apiSuccess {String}    name          ชื่อของ Product
@apiSuccess {String}    description   คำอธิบายของ Product
@apiSuccess {String}    unit          ชื่อหน่วยของ Product เช่น กล่อง, กระป๋อง
@apiSuccess {Datetime}  created_at    วันเวลาที่สร้าง Product
@apiSuccess {Datetime}  updated_at    วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Product
@apiSuccess {Object}    user          Object ของ user ที่เป็นเจ้าของ Product
@apiSuccess {Object}    category      Object ของ category คือประเภทของสินค้า
@apiSuccess {Object[]}  options       Object Array ของตัวเลือกสินค้า

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 1,
    "name": "Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook",
    "description": "content",
    "unit": "ชิ้น",
    "created_at": "2016-12-04T14:36:17+07:00",
    "updated_at": "2016-12-04T14:36:17+07:00",
    "user": {
      "id": 1,
      "provider": "email",
      "uid": "o_k_t@hotmail.com",
      "email": "o_k_t@hotmail.com",
      "name": "Tatchagon Koonkoei",
      "image": null,
      "birthday": "1993-06-02",
      "gender": "male",
      "phone_number": "0826810461",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "category": {
      "id": 1,
      "name": "อิเล็กทรอนิกส์",
      "description": "",
      "image": "",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "options": [
      {
        "id": 1,
        "name": "สีขาว",
        "price": 650,
        "quantity": 10,
        "weight": 500,
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      },
      {
        "id": 2,
        "name": "สีดำ",
        "price": 650,
        "quantity": 13,
        "weight": 500,
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    ]
  }

@apiError NotFound ไม่สามารถหา product ที่ตรงกับ <code>id</code> ได้

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

=end

# ============================= create ===========================
=begin

@api {post} /api/v1/products/ Create a product
@apiVersion 1.0.0
@apiName CreateAProduct
@apiGroup Product

@apiDescription สร้าง product ใหม่ โดยจะต้องมีการ sign in ก่อนจึงจะสามารถทำได้

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiParam (Product Param) {String}  product[name]           ชื่อของ Product
@apiParam (Product Param) {String}  [product[description]]  คำอธิบายของ Product
@apiParam (Product Param) {String}  [product[unit]]         ชื่อหน่วยของ Product เช่น กล่อง, กระป๋อง
@apiParam (Product Param) {Integer} product[user_id]        id  ของ user ที่เป็นเจ้าของ Product
@apiParam (Product Param) {Integer} product[category_id]    id  ของ category คือประเภทของสินค้า

@apiParamExample {json} ตัวอย่าง-Product-Param:
  {
    "product" : {
      "name": "Sky Coffee",
      "description": "content",
      "unit": "box",
      "user_id": 1,
      "category_id": 1
    }
  }

@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่ม Product ใหม่ได้

@apiErrorExample ตัวอย่าง-Validation-Error:
  HTTP/1.1 422 Unprocessable Entity
  {
    "user": [
      "must exist"
    ],
    "name": [
      "can't be blank"
    ]
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 201 Created
  {
    "id": 4,
    "name": "new product",
    "description": "content",
    "unit": "unit",
    "created_at": "2016-12-04T14:59:54+07:00",
    "updated_at": "2016-12-04T14:59:54+07:00",
    "user": {
      "id": 1,
      "provider": "email",
      "uid": "o_k_t@hotmail.com",
      "email": "o_k_t@hotmail.com",
      "name": "Tatchagon Koonkoei",
      "image": null,
      "birthday": "1993-06-02",
      "gender": "male",
      "phone_number": "0826810461",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "category": {
      "id": 1,
      "name": "อิเล็กทรอนิกส์",
      "description": "",
      "image": "",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "options": []
  }

=end

# ============================= update ===========================
=begin

@api {put} /api/v1/products/:id Update a product
@apiVersion 1.0.0
@apiName UpdateAProduct
@apiGroup Product

@apiDescription แก้ไขข้อมูล product โดยจะต้องมีการ sign in ก่อนจึงจะสามารถทำได้

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiParam (Product Param) {String}  product[name]           ชื่อของ Product
@apiParam (Product Param) {String}  [product[description]]  คำอธิบายของ Product
@apiParam (Product Param) {String}  [product[unit]]         ชื่อหน่วยของ Product เช่น กล่อง, กระป๋อง
@apiParam (Product Param) {Integer} product[user_id]        id  ของ user ที่เป็นเจ้าของ Product
@apiParam (Product Param) {Integer} product[category_id]    id  ของ category คือประเภทของสินค้า

@apiParamExample {json} ตัวอย่าง-Product-Param:
  {
    "product" : {
      "name": "Sky Coffee edit",
      "description": "content edit",
      "unit": "box edit",
      "user_id": 1,
      "category_id": 1
    }
  }

@apiError NotFound ไม่สามารถหา product ที่ตรงกับ <code>id</code> ได้
@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล Product ได้

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Validation-Error-Response:
  HTTP/1.1 422 Unprocessable Entity
  {
    "user": [
      "must exist"
    ],
    "name": [
      "can't be blank"
    ]
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 4,
    "name": "update product",
    "description": "content edit",
    "unit": "unit",
    "created_at": "2016-12-04T14:59:54+07:00",
    "updated_at": "2016-12-04T15:01:26+07:00",
    "user": {
      "id": 1,
      "provider": "email",
      "uid": "o_k_t@hotmail.com",
      "email": "o_k_t@hotmail.com",
      "name": "Tatchagon Koonkoei",
      "image": null,
      "birthday": "1993-06-02",
      "gender": "male",
      "phone_number": "0826810461",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "category": {
      "id": 1,
      "name": "อิเล็กทรอนิกส์",
      "description": "",
      "image": "",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    },
    "options": []
  }

=end

# ============================= destroy ===========================
=begin

@api {delete} /api/v1/products/:id Destroy a product
@apiVersion 1.0.0
@apiName DestroyAProduct
@apiGroup Product

@apiDescription ลบข้อมูล product โดยจะต้องมีการ sign in ก่อนจึงจะสามารถทำได้

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiError NotFound ไม่สามารถหา product ที่ตรงกับ <code>id</code> ได้

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 204 No Content

=end
