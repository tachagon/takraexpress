# ============================= index ===========================
=begin
@api {get} /api/v1/options Get all options
@apiVersion 1.0.0
@apiName GetOptions
@apiGroup Option

@apiDescription เรียกดูข้อมูลของตัวเลือกสินค้าทั้งหมด

@apiSuccess {Integer}   id            Id ของ Option
@apiSuccess {String}    name          ชื่อของ Option
@apiSuccess {Float}     price         ราคาสินค้าในตัวเลือกนั้น
@apiSuccess {Integer}   quantity      จำนวนสินค้าในตัวเลือกนั้น
@apiSuccess {Float}     weight        น้ำหนักของสินค้า
@apiSuccess {Datetime}  created_at    วันเวลาที่สร้าง Option
@apiSuccess {Datetime}  updated_at    วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Option
@apiSuccess {Object}    product       Object ของสินค้าที่เป็นเจ้าของตัวเลือก

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  [
    {
      "id": 1,
      "name": "สีขาว",
      "price": 650,
      "quantity": 10,
      "weight": 500,
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00",
      "product": {
        "id": 1,
        "name": "Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook",
        "description": "content",
        "unit": "ชิ้น",
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    },
    {
      "id": 2,
      "name": "สีดำ",
      "price": 650,
      "quantity": 13,
      "weight": 500,
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00",
      "product": {
        "id": 1,
        "name": "Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook",
        "description": "content",
        "unit": "ชิ้น",
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    },
    {
      "id": 3,
      "name": "White Gold",
      "price": 8250,
      "quantity": 20,
      "weight": 167,
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00",
      "product": {
        "id": 2,
        "name": "Samsung Galaxy J7 Prime",
        "description": "content",
        "unit": "เครื่อง",
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    },
    {
      "id": 4,
      "name": "Black",
      "price": 8250,
      "quantity": 12,
      "weight": 167,
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00",
      "product": {
        "id": 2,
        "name": "Samsung Galaxy J7 Prime",
        "description": "content",
        "unit": "เครื่อง",
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    },
    {
      "id": 5,
      "name": "Pink Gold",
      "price": 8250,
      "quantity": 15,
      "weight": 167,
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00",
      "product": {
        "id": 2,
        "name": "Samsung Galaxy J7 Prime",
        "description": "content",
        "unit": "เครื่อง",
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    },
    {
      "id": 6,
      "name": "size M",
      "price": 199,
      "quantity": 10,
      "weight": null,
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00",
      "product": {
        "id": 3,
        "name": "เดรสยาวลายริ้ว คอกลม แขนสั้น",
        "description": "content",
        "unit": "ตัว",
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    },
    {
      "id": 7,
      "name": "size L",
      "price": 199,
      "quantity": 8,
      "weight": null,
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00",
      "product": {
        "id": 3,
        "name": "เดรสยาวลายริ้ว คอกลม แขนสั้น",
        "description": "content",
        "unit": "ตัว",
        "created_at": "2016-12-04T14:36:17+07:00",
        "updated_at": "2016-12-04T14:36:17+07:00"
      }
    }
  ]

=end

# ============================= show ===========================
=begin

@api {get} /api/v1/options/:id Get a option
@apiVersion 1.0.0
@apiName GetAOption
@apiGroup Option

@apiDescription เรียกดูข้อมูลของ option ตาม id

@apiSuccess {Integer}   id            Id ของ Option
@apiSuccess {String}    name          ชื่อของ Option
@apiSuccess {Float}     price         ราคาสินค้าในตัวเลือกนั้น
@apiSuccess {Integer}   quantity      จำนวนสินค้าในตัวเลือกนั้น
@apiSuccess {Float}     weight        น้ำหนักของสินค้า
@apiSuccess {Datetime}  created_at    วันเวลาที่สร้าง Option
@apiSuccess {Datetime}  updated_at    วันเวลาล่าสุดที่มีการแก้ไขข้อมูล Option
@apiSuccess {Object}    product       Object ของสินค้าที่เป็นเจ้าของตัวเลือก

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 1,
    "name": "สีขาว",
    "price": 650,
    "quantity": 10,
    "weight": 500,
    "created_at": "2016-12-04T14:36:17+07:00",
    "updated_at": "2016-12-04T14:36:17+07:00",
    "product": {
      "id": 1,
      "name": "Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook",
      "description": "content",
      "unit": "ชิ้น",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    }
  }

@apiError NotFound ไม่สามารถหา option ที่ตรงกับ <code>id</code> ได้

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

=end

# ============================= create ===========================
=begin

@api {post} /api/v1/options/ Create a option
@apiVersion 1.0.0
@apiName CreateAOption
@apiGroup Option

@apiDescription สร้าง option ใหม่ โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็นเจ้าของสินค้าหรือผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiParam (Option Param) {String}   option[name]        ชื่อของตัวเลือกสินค้า
@apiParam (Option Param) {Float}    option[price]       ราคาสินค้าของตัวเลือกสินค้านี้
@apiParam (Option Param) {Integer}  option[quantity]    จำนวนสินค้าของตัวเลือกสินค้านี้
@apiParam (Option Param) {Float}    [option[weight]]    น้ำหนักสินค้าของตัวเลือกสินค้านี้
@apiParam (Option Param) {Integer}  option[product_id]  สินค้าที่เป็นเจ้าของตัวเลือกสินค้านี้

@apiParamExample {json} ตัวอย่าง-Product-Param:
  {
    "option" : {
      "name": "some option",
      "price": 99,
      "quantity": 25,
      "weight": 100,
      "product_id": 1
    }
  }

@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่ม Option ใหม่ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่มีสิทธิ์ในการดำเนินการนี้

@apiErrorExample ตัวอย่าง-Validation-Error:
  HTTP/1.1 422 Unprocessable Entity
  {
    "product": [
      "must exist"
    ],
    "name": [
      "can't be blank"
    ],
    "price": [
      "is not a number",
      "can't be blank"
    ],
    "quantity": [
      "is not a number",
      "can't be blank"
    ]
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 201 Created
  {
    "id": 8,
    "name": "some option",
    "price": 99,
    "quantity": 25,
    "weight": 100,
    "created_at": "2016-12-04T21:15:28+07:00",
    "updated_at": "2016-12-04T21:15:28+07:00",
    "product": {
      "id": 1,
      "name": "Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook",
      "description": "content",
      "unit": "ชิ้น",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    }
  }

=end

# ============================= update ===========================
=begin

@api {put} /api/v1/options/:id Update a option
@apiVersion 1.0.0
@apiName UpdateAOption
@apiGroup Option

@apiDescription แก้ไขข้อมูล option โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็นเจ้าของสินค้าหรือผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiParam (Option Param) {String}   option[name]        ชื่อของตัวเลือกสินค้า
@apiParam (Option Param) {Float}    option[price]       ราคาสินค้าของตัวเลือกสินค้านี้
@apiParam (Option Param) {Integer}  option[quantity]    จำนวนสินค้าของตัวเลือกสินค้านี้
@apiParam (Option Param) {Float}    [option[weight]]    น้ำหนักสินค้าของตัวเลือกสินค้านี้
@apiParam (Option Param) {Integer}  option[product_id]  สินค้าที่เป็นเจ้าของตัวเลือกสินค้านี้

@apiParamExample {json} ตัวอย่าง-Product-Param:
  {
    "option" : {
      "name": "update option",
      "price": 100,
      "quantity": 50,
      "weight": 100,
      "product_id": 1
    }
  }

@apiError NotFound ไม่สามารถหา option ที่ตรงกับ <code>id</code> ได้
@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล option ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่มีสิทธิ์ในการดำเนินการนี้

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Validation-Error-Response:
  HTTP/1.1 422 Unprocessable Entity
  {
    "product": [
      "must exist"
    ],
    "name": [
      "can't be blank"
    ],
    "price": [
      "is not a number",
      "can't be blank"
    ],
    "quantity": [
      "is not a number",
      "can't be blank"
    ]
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 8,
    "name": "update option",
    "price": 100,
    "quantity": 50,
    "weight": 100,
    "created_at": "2016-12-04T21:15:28+07:00",
    "updated_at": "2016-12-04T21:36:58+07:00",
    "product": {
      "id": 1,
      "name": "Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook",
      "description": "content",
      "unit": "ชิ้น",
      "created_at": "2016-12-04T14:36:17+07:00",
      "updated_at": "2016-12-04T14:36:17+07:00"
    }
  }

=end

# ============================= destroy ===========================
=begin

@api {delete} /api/v1/options/:id Destroy a Option
@apiVersion 1.0.0
@apiName DestroyAOption
@apiGroup Option

@apiDescription ลบข้อมูล option โดยจะต้องมีการ sign in ด้วยผู้ใช้ที่เป็นเจ้าของสินค้าหรือผู้ใช้ที่เป็น admin ก่อนจึงจะสามารถทำได้

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiError NotFound ไม่สามารถหา option ที่ตรงกับ <code>id</code> ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่มีสิทธิ์ในการดำเนินการนี้

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 204 No Content

=end