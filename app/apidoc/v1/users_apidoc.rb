# ============================= index ===========================
=begin
@api {get} /api/v1/users Get all users
@apiVersion 1.0.0
@apiName GetUsers
@apiGroup User

@apiDescription เรียกดูข้อมูลของ user ทั้งหมด

@apiSuccess {Integer}   id            Id ของ user
@apiSuccess {String}    provider      ถ้า user สมัครใช้งานด้วย email ก็จะมีค่าเป็น <code>email</code> แต่ถ้ามีสมัครด้วย Facebook ก็จะมีค่าเป็น <code>facebook</code>
@apiSuccess {String}    uid           มีค่าเดียวกับ email ของ user
@apiSuccess {String}    email         email ของ user
@apiSuccess {String}    name          ชื่อนามสกุลของ user
@apiSuccess {String}    image         ที่อยู่ของรูปภาพโปรไฟล์ของ user
@apiSuccess {Date}      birthday      วันเกิดของ user
@apiSuccess {String}    gender        เพศของ user มีค่าเป็น <code>male</code> หรือ <code>female</code> เท่านั้น
@apiSuccess {String}    phone_number  เบอร์โทรศัพท์ของ user มีค่าเป็นตัวเลขจำนวน 9 หรือ 10 หลักเท่านั้น
@apiSuccess {Datetime}  created_at    วันเวลาที่สร้าง user
@apiSuccess {Datetime}  updated_at    วันเวลาล่าสุดที่มีการแก้ไขข้อมูล user
@apiSuccess {Object}    role          Role ของ user
@apiSuccess {Object[]}  products      ข้อมูลสินค้าทั้งหมดของ user

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  [
    {
      "id": 1,
      "provider": "email",
      "uid": "o_k_t@hotmail.com",
      "email": "o_k_t@hotmail.com",
      "name": "Tatchagon Koonkoei",
      "image": null,
      "birthday": "1993-06-02",
      "gender": "male",
      "phone_number": "0826810461",
      "created_at": "2016-11-30T21:18:55+07:00",
      "updated_at": "2016-11-30T21:18:55+07:00",
      "role": {
        "id": 3,
        "name": "admin",
        "created_at": "2016-11-30T21:18:55+07:00",
        "updated_at": "2016-11-30T21:18:55+07:00"
      },
      "products": [
        {
          "id": 1,
          "name": "Sky Coffee",
          "description": "content",
          "unit": "box",
          "created_at": "2016-11-30T21:18:55+07:00",
          "updated_at": "2016-11-30T21:18:55+07:00"
        },
        {
          "id": 2,
          "name": "3D Nano",
          "description": "content",
          "unit": "can",
          "created_at": "2016-11-30T21:18:55+07:00",
          "updated_at": "2016-11-30T21:18:55+07:00"
        }
      ]
    },
    {
      "id": 2,
      "provider": "email",
      "uid": "tim_berner@email.com",
      "email": "tim_berner@email.com",
      "name": "Tim Berner",
      "image": null,
      "birthday": "1990-02-19",
      "gender": "male",
      "phone_number": "034658471",
      "created_at": "2016-11-30T21:18:55+07:00",
      "updated_at": "2016-11-30T21:18:55+07:00",
      "role": {
        "id": 1,
        "name": "member",
        "created_at": "2016-11-30T21:18:55+07:00",
        "updated_at": "2016-11-30T21:18:55+07:00"
      },
      "products": []
    }
  ]

=end

# ============================= show ===========================
=begin

@api {get} /api/v1/users/:id Get a user
@apiVersion 1.0.0
@apiName GetAUser
@apiGroup User

@apiDescription เรียกดูข้อมูลของ user ตาม id

@apiSuccess {Integer}   id            Id ของ user
@apiSuccess {String}    provider      ถ้า user สมัครใช้งานด้วย email ก็จะมีค่าเป็น <code>email</code> แต่ถ้ามีสมัครด้วย Facebook ก็จะมีค่าเป็น <code>facebook</code>
@apiSuccess {String}    uid           มีค่าเดียวกับ email ของ user
@apiSuccess {String}    email         email ของ user
@apiSuccess {String}    name          ชื่อนามสกุลของ user
@apiSuccess {String}    image         ที่อยู่ของรูปภาพโปรไฟล์ของ user
@apiSuccess {Date}      birthday      วันเกิดของ user
@apiSuccess {String}    gender        เพศของ user มีค่าเป็น <code>male</code> หรือ <code>female</code> เท่านั้น
@apiSuccess {String}    phone_number  เบอร์โทรศัพท์ของ user มีค่าเป็นตัวเลขจำนวน 9 หรือ 10 หลักเท่านั้น
@apiSuccess {Datetime}  created_at    วันเวลาที่สร้าง user
@apiSuccess {Datetime}  updated_at    วันเวลาล่าสุดที่มีการแก้ไขข้อมูล user
@apiSuccess {Object}    role          Role ของ user
@apiSuccess {Object[]}  products      ข้อมูลสินค้าทั้งหมดของ user

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 1,
    "provider": "email",
    "uid": "o_k_t@hotmail.com",
    "email": "o_k_t@hotmail.com",
    "name": "Tatchagon Koonkoei",
    "image": null,
    "birthday": "1993-06-02",
    "gender": "male",
    "phone_number": "0826810461",
    "created_at": "2016-11-30T21:18:55+07:00",
    "updated_at": "2016-11-30T21:18:55+07:00",
    "role": {
      "id": 3,
      "name": "admin",
      "created_at": "2016-11-30T21:18:55+07:00",
      "updated_at": "2016-11-30T21:18:55+07:00"
    },
    "products": [
      {
        "id": 1,
        "name": "Sky Coffee",
        "description": "content",
        "unit": "box",
        "created_at": "2016-11-30T21:18:55+07:00",
        "updated_at": "2016-11-30T21:18:55+07:00"
      },
      {
        "id": 2,
        "name": "3D Nano",
        "description": "content",
        "unit": "can",
        "created_at": "2016-11-30T21:18:55+07:00",
        "updated_at": "2016-11-30T21:18:55+07:00"
      },
      {
        "id": 3,
        "name": "AK-47",
        "description": "content",
        "unit": "gun barrel",
        "created_at": "2016-11-30T21:18:55+07:00",
        "updated_at": "2016-11-30T21:18:55+07:00"
      }
    ]
  }

@apiError NotFound ไม่สามารถหา user ที่ตรงกับ <code>id</code> ได้

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

=end

# ============================= create ===========================
=begin

@api {post} /api/v1/users/ Create a user
@apiVersion 1.0.0
@apiName CreateAUser
@apiGroup User

@apiDescription สร้าง user ใหม่

@apiParam {String}    user[name]                  ชื่อนามสกุลของ user
@apiParam {String}    user[email]                 email ของ user
@apiParam {Date}      [user[birthday]]            วันเกิดของ user
@apiParam {String}    [user[gender]]              เพศของ user มีค่าเป็น <code>male</code> หรือ <code>female</code> เท่านั้น
@apiParam {String}    [user[phone_number]]        เบอร์โทรศัพท์ของ user มีค่าเป็นตัวเลขจำนวน 9 หรือ 10 หลักเท่านั้น
@apiParam {String}    [user[image]]               ที่อยู่ของรูปภาพโปรไฟล์ของ user
@apiParam {String}    user[password]              รหัสผ่าน
@apiParam {String}    user[password_confirmation] ยืนยันรหัสผ่าน

@apiParamExample {json} ตัวอย่าง-User-Param:
  {
    "user" : {
      "name": "Sam Smith",
      "email": "email@example.com",
      "birthday": "02/06/1993",
      "gender": "male",
      "phone_number": "0826810461",
      "password": "password",
      "password_confirmation": "password"
    }
  }

@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถเพิ่ม User ใหม่ได้

@apiErrorExample ตัวอย่าง-Validation-Error:
  HTTP/1.1 422 Unprocessable Entity
  {
    "password": [
      "can't be blank"
    ],
    "email": [
      "can't be blank",
      "is not an email"
    ],
    "name": [
      "can't be blank"
    ]
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 201 Created
  {
    "id": 3,
    "provider": "email",
    "uid": "sam_smith@example.email.com",
    "email": "sam_smith@example.email.com",
    "name": "Sam Smith",
    "image": null,
    "birthday": "1993-06-02",
    "gender": "male",
    "phone_number": "0891234567",
    "created_at": "2016-11-30T23:05:47+07:00",
    "updated_at": "2016-11-30T23:05:47+07:00",
    "role": {
      "id": 1,
      "name": "member",
      "created_at": "2016-11-30T21:18:55+07:00",
      "updated_at": "2016-11-30T21:18:55+07:00"
    },
    "products": []
  }

=end

# ============================= update ===========================
=begin

@api {put} /api/v1/users/:id Update a user
@apiVersion 1.0.0
@apiName UpdateAUser
@apiGroup User

@apiDescription แก้ไขข้อมูล user โดยจะต้องเข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin หรือผู้ใช้ที่เป็นเจ้าของข้อมูลที่ต้องการแก้ไข

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiParam {String}    user[name]                    ชื่อนามสกุลของ user
@apiParam {String}    user[email]                   email ของ user
@apiParam {Date}      [user[birthday]]              วันเกิดของ user
@apiParam {String}    [user[gender]]                เพศของ user มีค่าเป็น <code>male</code> หรือ <code>female</code> เท่านั้น
@apiParam {String}    [user[phone_number]]          เบอร์โทรศัพท์ของ user มีค่าเป็นตัวเลขจำนวน 9 หรือ 10 หลักเท่านั้น
@apiParam {String}    [user[image]]                 ที่อยู่ของรูปภาพโปรไฟล์ของ user
@apiParam {String}    [user[password]]              รหัสผ่าน
@apiParam {String}    [user[password_confirmation]] ยืนยันรหัสผ่าน

@apiParamExample {json} ตัวอย่าง-User-Param:
  {
    "user" : {
      "name": "Sam Smith",
      "email": "email@example.com",
      "birthday": "02/06/1993",
      "gender": "male",
      "phone_number": "0826810461",
      "password": "password",
      "password_confirmation": "password"
    }
  }

@apiError NotFound ไม่สามารถหา user ที่ตรงกับ <code>id</code> ได้
@apiError UnprocessableEntity ข้อมูลที่ส่งมามีรูปแบบไม่ถูกต้องจึงไม่สามารถแก้ไขข้อมูล user ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin หรือไม่ได้เป็นเข้าของข้อมูลที่ต้องการแก้ไข

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Validation-Error-Response:
  HTTP/1.1 422 Unprocessable Entity
  {
    "password": [
      "can't be blank"
    ],
    "email": [
      "can't be blank",
      "is not an email"
    ],
    "name": [
      "can't be blank"
    ]
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 200 OK
  {
    "id": 3,
    "provider": "email",
    "uid": "marry_jane@example.com",
    "email": "marry_jane@example.com",
    "name": "Marry Jane",
    "image": null,
    "birthday": "1989-05-16",
    "gender": "famale",
    "phone_number": "0826810461",
    "created_at": "2016-11-30T23:05:47+07:00",
    "updated_at": "2016-11-30T23:38:26+07:00",
    "role": {
      "id": 1,
      "name": "member",
      "created_at": "2016-11-30T21:18:55+07:00",
      "updated_at": "2016-11-30T21:18:55+07:00"
    },
    "products": []
  }

=end

# ============================= destroy ===========================
=begin

@api {delete} /api/v1/users/:id Destroy a user
@apiVersion 1.0.0
@apiName DestroyAUser
@apiGroup User

@apiDescription ลบข้อมูล user โดยต้องเข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin เท่านั้น

@apiHeader (Header) {String} access-token ส่งข้อมูล access-token ที่ได้รับมาจาก Request ก่อน หรือได้รับครั้งแรกจากการ sign in
@apiHeader (Header) {String} client       ส่งข้อมูล client Token ที่ได้รับจากการ sign in
@apiHeader (Header) {String} uid          ส่งข้อมูล email ของ user

@apiHeaderExample {json} ตัวอย่าง-Header:
  {
    "access-token": "7iI0KfNDWrAiyIi3nL_ega",
    "client": "8Q8-EauC_ioAegsRS2S74b",
    "uid": "example@email.com"
  }

@apiError NotFound ไม่สามารถหา user ที่ตรงกับ <code>id</code> ได้
@apiError Forbidden เนื่องจากไม่ได้เข้าสู่ระบบด้วยผู้ใช้ที่เป็น admin

@apiErrorExample Error-Response:
  HTTP/1.1 404 Not Found
  {
    "error": "Not found"
  }

@apiErrorExample Forbidden-Error-Response:
  HTTP/1.1 403 Forbidden
  {
    "error": "Permission denied",
    "status": 403
  }

@apiSuccessExample Success-Response:
  HTTP/1.1 204 No Content

=end