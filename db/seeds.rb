require_relative('default_categories')
require_relative('th_location')

def create_province(name)
	Province.create(name: name)
end

def create_prefecture(name, province)
	Prefecture.create(name: name, province: province)
end

def create_district(name, prefecture, latitude, longitude)
	District.create(name: name, prefecture: prefecture, latitude: latitude, longitude: longitude)
end

def create_or_find_zipcode(code)
	zipcode = Zipcode.find_by_code(code)
	if zipcode.nil?
		return Zipcode.create(code: code)
	else
		return zipcode
	end
end

def create_district_zipcode(district, zipcode)
	DistrictZipcode.create(district: district, zipcode: zipcode)
end

def create_th_location
	provinces = th_location()
	count = 0
	provinces.each do |province, prefectures|
		province = create_province(province)
		prefectures.each do |prefecture, districts|
			prefecture = create_prefecture(prefecture, province)
			districts.each do |district, data|
				district = create_district(district, prefecture, data[:latitude], data[:longitude])
				data[:zipcode].each do |zipcode|
					zipcode = create_or_find_zipcode(zipcode)
					create_district_zipcode(district, zipcode)
				end
			end
		end
		count += 1
		puts "create Province #{count}/77 #{(count/77.0*100.0).round(2)}%"
	end
end

def create_default_role
	Role.create([
		{name: "member"},
		{name: 'producer'},
		{name: "admin"}
	])
end

def create_user(email, password, name, birthday, gender, phone_number, role=nil)
	User.create(
		email: email,
		password: password,
		name: name,
		birthday: birthday,
		gender: gender,
		phone_number: phone_number,
		role: role
	)
end

def create_category(name, description='', image='', supercategory=nil)
	Category.create(
		name: name,
		description: description,
		image: image,
		supercategory: supercategory
	)
end

def create_default_categories(categories, supercategory=nil)
  categories.each do |category|
    category.each do |name, data|
       new_category = create_category(name, data[:description], data[:image], supercategory)
      if data[:subcategories].any?
        create_default_categories(data[:subcategories], new_category)
      end
    end
  end
end

def create_product(name, unit, user, category, description="")
	Product.create(
		name: name,
		description: description,
		unit: unit,
		user: user,
		category: category
	)
end

create_th_location()
create_default_role()

create_user("o_k_t@hotmail.com", "password", "Tatchagon Koonkoei", "02/06/1993", "male", "0826810461", Role.admin)
create_user("tim_berner@email.com", "password", "Tim Berner", "19/02/1990", "male", "034658471")
create_user("leonado_davinci@email.com", "password", "Leonado Davinci", "01/10/1889", "male", "034658471", Role.producer)

categories = default_categories()
create_default_categories(categories)


product_1 = create_product("Remax Proda Power Bank 30000 mAh 4 Port รุ่น Notebook", "ชิ้น", User.first, Category.first, "content")
product_1.options.create(name: 'สีขาว', price: 650, quantity: 10, weight: 500)
product_1.options.create(name: 'สีดำ', price: 650, quantity: 13, weight: 500)

product_2 = create_product("Samsung Galaxy J7 Prime", "เครื่อง", User.last, Category.second, "content")
product_2.options.create(name: 'White Gold', price: 8250, quantity: 20, weight: 167)
product_2.options.create(name: 'Black', price: 8250, quantity: 12, weight: 167)
product_2.options.create(name: 'Pink Gold', price: 8250, quantity: 15, weight: 167)

product_3 = create_product("เดรสยาวลายริ้ว คอกลม แขนสั้น", "ตัว", User.first, Category.last, "content")
product_3.options.create(name: 'size M', price: 199, quantity: 10)
product_3.options.create(name: 'size L', price: 199, quantity: 8)
