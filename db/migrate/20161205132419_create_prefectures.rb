class CreatePrefectures < ActiveRecord::Migration[5.0]
  def change
    create_table :prefectures do |t|
      t.string :name
      t.references :province, foreign_key: true

      t.timestamps
    end
    add_index :prefectures, :name
    add_index :prefectures, [:name, :province_id], unique: true
  end
end
