class CreateOptions < ActiveRecord::Migration[5.0]
  def change
    create_table :options do |t|
      t.string :name
      t.float :price, default: 0.0
      t.integer :quantity, default: 0
      t.float :weight
      t.references :product, foreign_key: true

      t.timestamps
    end
  end
end
