def default_categories
  return [
    'อิเล็กทรอนิกส์' => {
      description: '',
      image: '',
      subcategories: [
        'โทรศัพท์มือถือ & แท็บเล็ต' => {
          description: '',
          image: '',
          subcategories: [
            'โทรศัพท์มือถือ' => {
                description: '',
                image: '',
                subcategories: []
            },
            'แท็บเล็ต' => {
                description: '',
                image: '',
                subcategories: []
            },
            'อุปกรณ์เสริมโทรศัพท์มือถือ' => {
                description: '',
                image: '',
                subcategories: [
                  'แบตเตอรีสำรอง' => {
                      description: '',
                      image: '',
                      subcategories: []
                  },
                  'แบตเตอรี และอุปกรณ์ชาร์จไฟ' => {
                      description: '',
                      image: '',
                      subcategories: []
                  },
                  'เคสและซองมือถือ' => {
                      description: '',
                      image: '',
                      subcategories: []
                  },
                ]
            },
          ]
        }
      ]
    },
    # / อิเล็กทรอนิกส์
    'แฟชั่นสุภาพสตรี' => {
        description: '',
        image: '',
        subcategories: []
    },
  ]
end

=begin
'' => {
    description: '',
    image: '',
    subcategories: []
},
=end
